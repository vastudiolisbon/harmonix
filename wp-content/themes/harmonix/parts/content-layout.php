<?php

if( have_rows('modules', $post->ID) ): ?>
      <?php
        while ( have_rows('modules') ) :
          the_row();
            if( get_row_layout() == 'quote' ): ?>
            <section class="container-fluid content quote">
                <div class="row">
                    <div class="col-md-12 d-flex flex-column justify-content-center align-items-center">
                      <div class=" text-center">
                        <?php echo get_sub_field('quote_block'); ?>
                      </div>
                      <?php if(!empty(get_sub_field('author'))){ ?>
                        <p class="signature"><?php echo get_sub_field('author'); ?></p>
                      <?php } ?>

                    </div>
                </div>
              </section>
              <?php
              elseif (get_row_layout() == 'text_block'): ?>
              <section class="container-fluid content quote" <?php if( get_sub_field('background_color') ){echo 'style="background-color:'. get_sub_field('background_color') .'"';} ?>>
                  <div class="row">
                      <div class="col-md-12 d-flex flex-column justify-content-center align-items-center">
                        <div class="text-block text-center">
                          <?php echo get_sub_field('text_content'); ?>
                        </div>
                      </div>
                  </div>
                </section>
            <?php
            elseif (get_row_layout() == 'two_columns'): ?>
            <section class="container-fluid content">
              <?php
                $left_column = get_sub_field('left_column');
                $left_column_type = $left_column['left_column_type'];
                ?>

              <div class="row no-gutters <?php if($left_column_type == 'text'): echo 'img-inverse-row'; endif; ?>">
                <?php

                  if(!empty($left_column['background_color']) && $left_column['left_column_type'] == 'text'){
                    $column_style =  'style="background-color:'.$left_column['background_color'].'"';
                  }else{
                    $column_style = '';
                  }
                ?>
                <div class="col-md-6" <?php echo $column_style; ?>>
                  <?php

                    render_columns($left_column_type, $left_column);
                  ?>
                </div>

                <?php
                  $right_column = get_sub_field('right_column');

                  if(!empty($right_column['background_color']) && $right_column['column_type'] == 'text'){
                    $column_style = 'style="background-color:'.$right_column['background_color'].'"';
                  }else{
                    $column_style = '';
                  }
                ?>
                <div class="col-md-6" <?php echo $column_style; ?>>
                
                  <?php
                    $right_column_type = $right_column['column_type'];
                    render_columns($right_column_type, $right_column);
                  ?>
                </div>
              </section>
              <?php
              elseif (get_row_layout() == 'values'): ?>
              <section class="container-fluid content values" style="background-color:<?php echo get_sub_field('background_color'); ?>">
                  <div class="row">
                      <div class="col-12 text-left">
                        <p class="h2"><?php echo get_sub_field('title'); ?></p>
                      </div>

                      <?php
                      
                      if( have_rows('values') ):
                        echo '<div class="col-12 values-feed"><div class="row">';
                         while ( have_rows('values') ) : the_row();
                         echo '<div class="values-single col-12 col-md-4">';
                         echo '<div class="value-wrapper">';
                         echo '<h4><i class="arrow-values mr-2 "><img src="' . get_template_directory_uri() . '/assets/img/arrow-values.svg"></i>' . get_sub_field('title') . '</h4>';
                         echo get_sub_field('description');
                         echo '</div></div>';
                       endwhile;
                       echo '</div></div>';
                     endif;

                      ?>  
                  </div>
                </section>
            <?php
            endif;
        endwhile;
      ?>

<?php endif;  ?>
