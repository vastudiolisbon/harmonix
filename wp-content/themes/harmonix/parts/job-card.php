<?php
foreach ($jobs as $job) {
  $title = get_the_title($job->ID);
  $company_type = get_the_terms($job->ID, 'job-category');
?>


  <div class="portfolio-item job-item <?php echo $category->slug; ?>-filter <?php echo $category->slug; ?>" data-company-type="<?php if ($company_type) : echo $company_type[0]->slug; endif; ?>">
    <a href="<?php echo get_the_permalink($job->ID); ?>" class="job-card">
      <div class="job-card-content">
        <h2 class="name h4"><?php echo get_the_title($job->ID); ?></h2>
        <p class="subtitle"><?php echo get_field('company', $job->ID); ?></p>
        <div class="portfolio-thumb-description">
            <svg width="19.39" height="19.39" version="1.1" x="0px" y="0px" viewBox="0 0 68.7 68.4" style="enable-background:new 0 0 68.7 68.4;" xml:space="preserve">
              <style type="text/css">.st0{stroke-width:2;stroke-linecap:round;}</style>
              <g>
              	<g id="Group_28988" transform="translate(694.914 -1495.5) rotate(90)">
              		<line id="Line_11933" class="st0" x1="1562.7" y1="660.6" x2="1496.7" y2="660.6"/>
              	</g>
              	<g id="Group_28988_1_" transform="translate(694.914 -1495.5) rotate(90)">
              		<line id="Line_11933_1_" class="st0" x1="1529.7" y1="627.7" x2="1529.7" y2="693.6"/>
              	</g>
              </g>
            </svg>
        </div>
      </div>
    </a>
  </div>
<?php
}
?>
