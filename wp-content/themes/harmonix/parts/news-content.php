<div class="grid-page-container bg-light container-fluid media-single">
  <div class="row">
    <div class="back-to-articles col-12">
      <p class="col-sm-12 page-title h6 font-unica"><a href="/media">
      
        <svg class="back-arrow" width="29" height="20" viewBox="0 0 29 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M1 10L28 10" stroke="black" stroke-linecap="round"/>
          <path d="M1.02702 9.99902L9.99902 1.00002" stroke="black" stroke-linecap="round"/>
          <path d="M1.00002 9.99905L9.99902 18.998" stroke="black" stroke-linecap="round"/>
        </svg>

      Back to Media</a></p>
    </div>
    <div class="col-12">
      <article class="row">
        <div class="col-12">

          <p class="subtitle"><?php echo get_the_date();?></p>
        </div>
        <div class="col-12">

          <h1 class="h2"><?php the_title();?></h1>
        </div>
        <div class="col-12">
          <?php
      $author = get_field('author');
      if( $author ): ?>
          <?php if ($author['image']) :?>
             <img class="author-image" src="<?php echo $author['image']['sizes']['medium']?>">
          <?php else:?>
              <p class="author"><?php echo $author['name']; ?></p>
          <?php endif;?>
      <?php endif; ?>
        </div>
        <?php if(has_post_thumbnail()):?>
        <div class="col-12">
        <img class="featured-image" src="<?php echo get_the_post_thumbnail_url();?>">
        </div>
        <?php else:?>
          <div class="col-12 my-4"></div>
        <?php endif;?>
        <div class="col-12 post-content">
            <?php echo get_the_content( );?>
        </div>
      </article>
    </div>
  </div>
</div>
