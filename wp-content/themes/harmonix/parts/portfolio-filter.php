
<ul class="portfolio-filter clearfix">
  <?php
    $cats = get_terms(['taxonomy' => 'portfolio-fund', 'hide_empty' => false, 'meta_key' => 'tax_position', 'orderby' => 'tax_position' ]);
    $firstLoop = true;
    foreach ($cats as $key => $category) {
      ?>
      <li class="<?php if($firstLoop){ echo 'active'; $firstLoop = false; } ?>" data-filter=".fund-<?php echo $category->slug; ?>" ><?php echo $category->name; ?></li>
      <?php
    }
  ?>
</ul>
