<?php
    $title = get_the_title();
    $company_type = get_field('category');
    $company_status = get_field('status');
    $company_entry = get_field('entry_stage');
    $logo_id = get_field('logo')['id'];
    $logo = wp_get_attachment_image_src($logo_id, 'medium');
    $fund_category = get_field('fund');
    $highlight = get_field('card_text')
?>

<div class="portfolio-item <?php if( $fund_category && is_array($fund_category) ) {foreach( $fund_category as $term ) { echo 'fund-'.$term->slug. ' '; }}?><?php if( $company_type && is_array($company_type) ) {foreach( $company_type as $term ) { echo 'type-'.$term->slug. ' '; }}?><?php if( $company_entry && is_array($company_entry) ) {foreach( $company_entry as $term ) { echo 'entry-'.$term->slug. ' '; }}?><?php if( $company_status && is_array($company_status) ) {foreach( $company_status as $term ) { echo 'status-'.$term->slug. ' '; }}?>"
<?php if( $fund_category && is_array($fund_category) ):?>data-company-fund="<?php foreach( $fund_category as $term ) { echo esc_html( $term->slug ); } ?>"<?php endif;?>
<?php if( $company_type && is_array($company_type) ):?>data-company-type="<?php foreach( $company_type as $term ) { echo esc_html( $term->slug ); } ?>"<?php endif;?>
<?php if( $company_entry && is_array($company_entry) ):?>data-company-entry="<?php foreach( $company_entry as $term ) { echo esc_html( $term->slug ); } ?>"<?php endif;?>
<?php if( $company_status && is_array($company_status) ):?>data-company-status="<?php foreach( $company_status as $term ) { echo esc_html( $term->slug ); } ?>"<?php endif;?>
  >
    <span class="highlight"><?php echo $highlight;?></span>
    <h3 class="name"><?php echo $title; ?></h3>
   <img class="portfolio-logo" src="<?php echo $logo[0]; ?>" alt="<?php echo $title; ?>">
   <img class="portfolio-logo-hover" src="<?php echo $logo[0]; ?>" alt="<?php echo $title; ?>">
    <div class="portfolio-thumb-description">
      <p class="subtitle"><?php echo get_field('description', $portfolio_item->ID); ?></p>
      <?php if(!empty(get_field('website', $portfolio_item->ID))){ ?>
        <a href="<?php echo get_field('website', $portfolio_item->ID); ?>" target="_blank" rel="noopener">
          <svg width="19.39" height="19.39" version="1.1" x="0px" y="0px" viewBox="0 0 19.4 19.4" style="enable-background:new 0 0 19.4 19.4;" xml:space="preserve">
            <g transform="translate(0.5 0.5)">
            	<path class="st0" d="M15.3,10.7v7.7H0V3.1h7.7"/>
            	<path class="st0" d="M10.7,0h7.7v7.7"/>
            	<line class="st0" x1="9.2" y1="9.2" x2="18.4" y2="0"/>
            </g>
          </svg>
        </a>
      <?php } ?>
    </div>

    <div class="portfolio-expandable">
      <div class="d-flex align-items-start flex-column">
        <div class="company-details">
          <img src="<?php echo $logo[0]; ?>" alt="<?php echo $title; ?>">
          <?php if($highlight):?>
            <div class="highlight-expanded">
              <p class="text-brand-color"><?php echo $highlight;?></p>
            </div>
          <?php endif;?>
          <div class="description"><?php echo get_field('description', $portfolio_item->ID); ?></div>
          <?php if(!empty(get_field('website', $portfolio_item->ID))){ ?>
            <a class="button" href="<?php echo get_field('website', $portfolio_item->ID); ?>" target="_blank" rel="noopener">
              Visit website
            </a>
          <?php }else{ ?>
          <?php } ?>
        </div>
        <div class="company-stats mt-auto">
          <div class="row">
          <?php if ($company_entry && is_array($company_entry)):?>
            <div class="col-12 col-md-auto mb-2">
              <span class="button">Entry Stage: 
              <?php foreach($company_entry as $tax) echo '<span class="status-pill">' . $tax->name . '</span>'; ?></span>
            </div>
            <?php endif;?>
            <?php if ($company_status && is_array($company_status)):?>
            <div class="col-12 col-md-auto mb-2">
              <span class="button">Status: 
              <?php foreach($company_status as $tax) echo '<span class="status-pill">' . $tax->name . '</span>'; ?></span>
            </div>
            <?php endif;?>
            <?php if ($company_type && is_array($company_type)):?>
            <div class="col-12 col-md-auto mb-2">
              <span class="button">Sector: 
              <?php foreach($company_type as $tax) echo '<span class="status-pill">' . $tax->name . '</span>'; ?></span>
            </div>
            <?php endif;?>
          </div>
        </div>
      </div>
      <button class="popup-close"></button>
    </div>
  </div>
