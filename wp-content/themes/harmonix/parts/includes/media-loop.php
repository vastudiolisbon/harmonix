<div class="media-item col-md-6 col-lg-4">
    <?php if(has_post_thumbnail()):?>
        <a href="<?php echo get_permalink();?>">
            <div class="featured-image">
                <img src="<?php echo get_the_post_thumbnail_url();?>">
            </div>
        </a>
    <?php endif;?>
   <div class="media-info">
    <span class="subtitle date"><?php echo get_the_date(); ?></span>
    <a href="<?php echo get_permalink();?>">
    <h3 class="<?php if(has_post_thumbnail()){echo 'h5';}else{ echo 'h4';};?> title"><?php echo get_the_title();?></h3>
    </a>
    <?php
    $author = get_field('author');
    if( $author ): ?>
        <?php if ($author['image']) :?>
           <img class="author-image" src="<?php echo $author['image']['sizes']['medium']?>">
        <?php else:?>
            <p class="author"><?php echo $author['name']; ?></p>
        <?php endif;?>
    <?php endif; ?>
    <p class="excerpt"><?php echo get_the_excerpt( )?></p>
    <a class="btn button btn-pill" href="<?php echo get_permalink()?>">Read More</a>
   </div>
</div>