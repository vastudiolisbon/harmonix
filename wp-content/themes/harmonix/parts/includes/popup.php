<div class="popup">
    <div class="container-fluid">
        <div class="row">
            <div class="col-10 col-md-6 col-lg-7 order-1">
                <h3 class="popup-title">Join our newsletter</h3>
            </div>
            <div class="col-md-5 col-lg-4 order-3 order-md-2">
                <!-- Begin Mailchimp Signup Form -->
                <div id="popup-mc_embed_signup">
                    <form action="https://harmonixfund.us2.list-manage.com/subscribe/post?u=89d78f86888061e6857ad8f95&amp;id=46f0f52781" method="post" id="subscribe-form-2" name="mc-embedded-subscribe-form" class="mailchimp-form" target="_blank" novalidate>
                        <div class="mc-field-group">
                            <div class="d-flex justify-content-between align-items-center">
                                <input type="email" value="" name="EMAIL" class="required email" id="popup-mce-EMAIL" />
                                <button type="submit" name="subscribe" id="popup-mc-embedded-subscribe" class="button"><span class="arrow-submit"></span> </button>
                            </div>
                        </div>
                        <div id="popup-mce-responses" class="clear">
                            <div class="response" id="popup-mce-error-response" style="display:none"></div>
                            <div class="response" id="popup-mce-success-response" style="display:none"></div>
                        </div>
                         <div id="subscribe-result-2"></div>

                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_89d78f86888061e6857ad8f95_46f0f52781" tabindex="-1" value=""></div>
                    
                    </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            <div class="col-2 col-md-1 d-flex justify-content-end order-2 order-md-3">
                <button class="popup-close"></button>
            </div>
        </div>
    </div>    
</div>