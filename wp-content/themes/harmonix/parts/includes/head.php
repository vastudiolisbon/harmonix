  <?php
  if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
  }
  ?>
  <!DOCTYPE html>
  <html <?php language_attributes(); ?>>
  <?php
  $thumbnail = get_template_directory_uri() . '/Screenshot.png';

  if (is_home() || is_front_page()) {
    $title = get_bloginfo('name');
    $description = get_bloginfo('description');
  } elseif (is_single() || is_page()) {

    $title = get_the_title($post->ID) . ' - ' . get_bloginfo('name');

    // if(!empty(get_field('description', $post->ID))){
    //   $description = get_field('description', $post->ID);
    // }elseif(!empty(get_field('biography', $post->ID))){
    //   $description = get_field('biography', $post->ID);
    // }elseif(!empty(get_the_excerpt($post->ID))){
    //   $description = get_the_excerpt($post->ID);
    // }else{
    //   $description = get_bloginfo('description');
    // }

    if (!empty(get_the_post_thumbnail($post->ID))) :
      $thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
      $thumbnail = $thumbnail_src[0];
    elseif (!empty(get_field('logo', $post->ID))) :
      $thumbnail_src = wp_get_attachment_image_src(get_field('logo', $post->ID), 'medium');
      $thumbnail = $thumbnail_src[0];
    elseif (!empty(get_field('photo', $post->ID))) :
      $thumbnail_src = wp_get_attachment_image_src(get_field('photo', $post->ID), 'medium');
      $thumbnail = $thumbnail_src[0];
    else :
      $thumbnail = get_template_directory_uri() . '/Screenshot.png';
    endif;
  } elseif (is_tax()) {
    $tax = get_queried_object();
    $title = $tax->name . ' - ' . get_bloginfo('name');
    $description = $tax->description;
  } elseif (is_post_type_archive('portfolio')) {
    $title = 'Companies - ' . get_bloginfo('name');
    $description = get_bloginfo('description');
    $archive = get_queried_object();
    $link = get_post_type_archive_link(get_queried_object()->name);
  } elseif (is_archive()) {
    $archive = get_queried_object();
    $title = $archive->label . ' - ' . get_bloginfo('name');
    $description = get_bloginfo('description');
    $link = get_post_type_archive_link(get_queried_object()->name);
  } elseif (is_404()) {
    $title = '404 - ' . get_bloginfo('name');
    $description = get_bloginfo('description');
  }
  ?>

  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $description; ?>">
    <meta property="og:locale" content="en" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:description" content="<?php echo $description; ?>" />
    <meta property="og:url" content="<?php if (is_archive()) : echo $link;
                                      else : get_the_permalink($post->ID);
                                      endif; ?>" />
    <meta property="og:site_name" content="<?php echo get_bloginfo('name'); ?>" />
    <meta property="og:image" content="<?php echo $thumbnail;  ?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?php echo $title; ?>" />
    <meta name="twitter:image" content="<?php echo $thumbnail; ?>" />
    <script src="https://player.vimeo.com/api/player.js"></script>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>