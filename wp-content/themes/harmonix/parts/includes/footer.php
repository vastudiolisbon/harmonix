</main>
  <?php

    if (!defined('ABSPATH')) {
      exit; // Exit if accessed directly
    }
    
    echo wp_footer();
    if(is_home() || is_front_page()){
      $footer_class = 'light';
    }else{
      $footer_class = '';
    }
  ?>
<footer class="dark py-5">
  <div class="container-fluid pb-5">
    <div class="row">
      <div class="col-6">
        <svg version="1.1" class="logo-sm" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          width="912.9" height="149.1" viewBox="0 0 912.9 149.1" style="enable-background:new 0 0 912.9 149.1;" xml:space="preserve">
        <g>
          <path class="st0" d="M0,147.3V13.4h22.4V71h66V13.4h22.4v133.9H88.4V88.2h-66v59.1H0z"/>
          <path class="st0" d="M132.8,123.4c0-11.7,5.1-18.5,13.2-23.5c7.2-4.6,16.7-7.2,34.3-10.8c18.3-3.9,24.4-7.2,24.4-14.7
            s-5.7-14.3-21.1-14.3c-14.7,0-23.8,7.7-28.4,20.9l-20-6.4c8.6-21.3,26.8-32.1,49.5-32.1c25.7,0,41.4,12.7,41.4,36.7v68H205v-23.8
            c-6.6,16.1-21.8,25.7-40,25.7C144.6,149.1,132.8,139,132.8,123.4z M205.6,99.6v-7.9c-3.1,4.2-9.9,6.8-23.3,9.7
            c-18.3,3.9-27.1,7.9-27.1,18.3c0,8.1,6.4,12.6,18.3,12.6C191.7,132.4,205.1,116.6,205.6,99.6z"/>
          <path class="st0" d="M321.2,47.1L315,67.5c-5-3.1-10.6-4.6-17.4-4.6c-12.7,0-22.6,10.8-22.6,32.3v52.1h-22.2V44.6H275v24.8
            c4.6-17.8,13.6-26.2,28.4-26.2C311,43.1,316.7,44.7,321.2,47.1z"/>
          <path class="st0" d="M359.6,147.3h-22.2V44.6h22.2v24c6.4-17.4,18.3-26,34.5-26c17.4,0,28.8,9.7,32.6,25.7
            c6.8-16.9,19.8-25.7,35.9-25.7c11.4,0,20,3.9,26,11.7c6.1,7.7,9.2,17.8,9.2,30.1v62.9h-22.2V90c0-17.6-5.1-29-22-29
            c-16.1,0-24.9,12.6-24.9,35.2v51h-22.1V89.8c0-17.6-5.1-28.8-22-28.8c-16.1,0-24.9,13-24.9,35.6v50.7H359.6z"/>
          <path class="st0" d="M519.1,95.9c0-30.1,19.6-53.4,52.6-53.4c33.4,0,52.4,23.3,52.4,53.4c0,29.7-19.6,53.2-52.4,53.2
            C537.8,149.1,519.1,126.4,519.1,95.9z M601.4,95.9c0-20.5-9.2-34.8-29.7-34.8c-10.1,0-17.6,3.3-22.6,9.9c-5,6.6-7.3,14.9-7.3,24.9
            c0,20.4,8.8,34.3,29.9,34.3C592.3,130.2,601.4,115.9,601.4,95.9z"/>
          <path class="st0" d="M646.3,147.3V44.6h22.2V69c6.8-17.6,19.6-26.4,36.5-26.4c11.9,0,21.1,3.9,27.7,11.7c6.6,7.7,9.9,17.8,9.9,30.3
            v62.7h-22.4v-57c0-17.6-6.6-29.2-24.8-29.2c-16.3,0-27,13-27,35.6v50.6L646.3,147.3L646.3,147.3z"/>
          <path class="st0" d="M781.1,0c9,0,14.5,6.1,14.5,14.7c0,8.3-5.5,14.3-14.5,14.3c-9.4,0-14.5-6-14.5-14.3C766.6,6.1,771.8,0,781.1,0
            z M791.8,44.6v102.7h-22.4V44.6H791.8z"/>
          <path class="st0" d="M806.8,147.3L847,94.5l-37.8-49.9h25.1l26,35.4l26-35.4h24.6l-38,49.9l40,52.8h-24.6L859.9,109l-28.2,38.3
            L806.8,147.3L806.8,147.3z"/>
        </g>
        </svg>
      </div>
      <div class="col-6 d-flex justify-content-end">
        <div class="logo-lg"> <svg xmlns="http://www.w3.org/2000/svg" width="25.415" height="30.303" viewBox="0 0 25.415 30.303"><path d="M1059.707-737.755h.488v-7.332a5.377,5.377,0,0,0-5.376-5.378h-9.775a4.393,4.393,0,0,1-3.111-1.287,4.394,4.394,0,0,1-1.288-3.111V-767.57h-5.865v7.82a5.375,5.375,0,0,0,5.376,5.376h9.775a4.384,4.384,0,0,1,3.11,1.289,4.386,4.386,0,0,1,1.289,3.111v12.708h5.864v-.488h-.488v-.49h-4.4v-11.729a5.375,5.375,0,0,0-5.376-5.376h-9.775a4.393,4.393,0,0,1-3.111-1.289,4.384,4.384,0,0,1-1.287-3.11v-6.843h3.907l0,6.843v4.888a5.375,5.375,0,0,0,5.376,5.376h9.775a4.385,4.385,0,0,1,3.11,1.289,4.383,4.383,0,0,1,1.287,3.111v7.332h.49v0Z" transform="translate(-1034.78 767.57)" fill="#4870e9"/><path d="M1046.236-767.081h-.488v9.775a2.931,2.931,0,0,0,2.932,2.932,2.932,2.932,0,0,0,2.932-2.932V-767.57h-5.864v.489h.488v.488h4.4v9.287a1.959,1.959,0,0,1-1.954,1.956,1.959,1.959,0,0,1-1.955-1.956v-9.775h-.489v0Z" transform="translate(-1026.197 767.57)" fill="#4870e9"/><path d="M1040.156-745.265h.488v-9.776a2.933,2.933,0,0,0-2.932-2.934,2.933,2.933,0,0,0-2.932,2.934v10.264h5.865v-.488h-.488v-.49h-4.4v-9.285a1.957,1.957,0,0,1,1.954-1.955,1.957,1.957,0,0,1,1.955,1.955v9.776h.489v0Z" transform="translate(-1034.78 775.079)" fill="#4870e9"/></svg></div>
      </div>
    </div>
    <div class="footer-bottom row flex-md-row-reverse">
      <div class="footer-nav-container col-md-7 col-lg-8 d-md-flex flex-wrap justify-content-md-end align-items-md-end flex-column menu">
        <div class="d-lg-flex flex-lg-row align-items-end">
        <?php
          wp_nav_menu([
            'menu' => 'footer-menu',
          ]);

          $social_profiles = get_field('social_media_profiles', 'option');

          ?>
          <ul>
          <?php
          foreach ($social_profiles as  $social_profile) { ?>

            <li><a href="<?php echo $social_profile['link']; ?>" target="_blank" rel="noopener"><?php echo $social_profile['link_text']; ?></a></li>

          <?php  } ?>
          </ul>
        </div>
          <!-- 78015 Main Street, Suite 206B, La Quinta, CA 92253  -->
      </div>

      <div class="col-md-5 col-lg-4 d-flex align-items-end">
        <!-- Begin Mailchimp Signup Form -->
        <div id="mc_embed_signup">
          <form action="https://harmonixfund.us2.list-manage.com/subscribe/post?u=89d78f86888061e6857ad8f95&amp;id=46f0f52781" method="post" id="subscribe-form" name="mc-embedded-subscribe-form" class="mailchimp-form" target="_blank" novalidate>
              <label for="mce-EMAIL">Join our newsletter</label>
                <div class="mc-field-group">

                  <div class="d-flex justify-content-between align-items-center">
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" />
                    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">Subscribe</button>
                  </div>
                </div>
                <div id="mce-responses" class="clear">
                  <div class="response" id="mce-error-response" style="display:none"></div>
                  <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
                <div id="subscribe-result"></div>

              <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_89d78f86888061e6857ad8f95_46f0f52781" tabindex="-1" value=""></div>
              </div>

          </form>
        </div>
        <!--End mc_embed_signup-->
      </div>
    </div>
  </div>
</footer>
<?php include "popup.php" ?>
</body>
</html>
