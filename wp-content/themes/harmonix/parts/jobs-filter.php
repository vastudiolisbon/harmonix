
<ul class="portfolio-filter clearfix">
  <li class="filter-all <?php if(!$tax){ echo 'active'; } ?>" data-filter=".portfolio-item:not(.uncategorized-filter), .portfolio-item-heading">All</li>
  <?php
    $categories = get_terms(['taxonomy' => 'job-category', 'hide_empty' => false, 'meta_key' => 'tax_position', 'orderby' => 'tax_position' ]);

    foreach ($categories as $key => $category) { ?>
      <li class="<?php if($tax == $category->term_id){ echo 'active'; } ?>" data-filter=".<?php echo $category->slug; ?>" ><?php echo $category->name; ?></li>
  <?php
    }
  ?>

</ul>
