<?php

if(!empty(get_field('graph_section', $post->ID))){

  $graph_section = get_field('graph_section', $post->ID);
  $graph_desktop_image = wp_get_attachment_image_src($graph_section['graph'], 'full')[0];

  if(!empty($graph_section['mobile_graph'])){
    $graph_mobile_image = wp_get_attachment_image_src($graph_section['mobile_graph'], 'medium_large')[0];
  }else{
    $graph_mobile_image = $graph_desktop_image;
  }

  $graph_image_alt = get_post_meta($graph_section['graph'], '_wp_attachment_image_alt', true);
  $graph_button = $graph_section['button_options'];
?>
 <div class="graph-section bg-light">
   <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <h2><?php echo $graph_section['title']; ?></h2>
        <p><?php echo $graph_section['subtitle']; ?></p>
        <img src="<?php echo $graph_desktop_image; ?>" alt="<?php echo $graph_image_alt; ?>" class="desktop-image">
        <img src="<?php echo $graph_mobile_image; ?>" alt="<?php echo $graph_image_alt; ?>" class="mobile-image">
      <?php
        if(!empty($graph_button)){ ?>
          <a class="pill" href="<?php echo $graph_button['button_link']; ?>" <?php if($graph_button['external_link']){ ?> target="_blank" rel="nooperner" <?php } ?>>
            Learn More
        </a>
      <?php } ?>
        </div>
      </div>
    </div>
  </div>

<?php
  }
?>
