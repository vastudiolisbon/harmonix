<?php

if(!empty(get_field('pipeline_progress', $post->ID))){

  $pipeline_section = get_field('pipeline_progress', $post->ID);
  $pipeline_desktop_image = wp_get_attachment_image_src($pipeline_section['desktop_image'], 'full')[0];

  if(!empty($pipeline_section['mobile_image'])){
    $pipeline_mobile_image = wp_get_attachment_image_src($pipeline_section['mobile_image'], 'medium_large')[0];
  }else{
    $pipeline_mobile_image = $pipeline_desktop_image;
  }

  $pipeline_image_alt = get_post_meta($pipeline_section['desktop_image'], '_wp_attachment_image_alt', true);

?>
 <div class="graph-section bg-light">
   <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <h2><?php echo $pipeline_section['title']; ?></h2>
        <img src="<?php echo $pipeline_desktop_image; ?>" alt="<?php echo $pipeline_image_alt; ?>" class="desktop-image">
        <img src="<?php echo $pipeline_mobile_image; ?>" alt="<?php echo $pipeline_image_alt; ?>" class="mobile-image">
        </div>
      </div>
    </div>
  </div>

<?php
  }
?>
