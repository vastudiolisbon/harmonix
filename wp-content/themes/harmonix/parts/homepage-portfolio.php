<?php

if (!empty(get_field('portfolio_section', $post->ID))) {

  $portfolio = get_field('portfolio_section', $post->ID);
  $cover_info = $portfolio['cover_info'];

  if ($cover_info['media_type'] == 'image') {
    $background_image_id = $cover_info['image'];
    $background_image = wp_get_attachment_image_src($background_image_id, 'full')[0];
    $background_image_alt = get_post_meta($background_image_id, '_wp_attachment_image_alt', true);
  } else {
    $background_image = '';
  }
?>

  <div style="background-image:url('<?php echo $background_image; ?>')" class="portfolio-section">
    <div class="layer-1 align-items-end d-flex">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 ">

            <h2><a href="<?php echo $portfolio['link']; ?>"><?php echo $portfolio['title']; ?></a></h2>

            <a class="pill pill-light" href="<?php echo $portfolio['link']; ?>"><?php echo $portfolio['button_text']; ?></a>
          </div>
        </div>
      </div>
    </div>

    <?php
    if ($cover_info['media_type'] == 'video') {
      $video_url = $cover_info['video'];
    ?>
      <div class="video-wr">
        <video class="video-portfolio" autoplay muted loop playsinline>
          <source src="<?php echo $video_url; ?>" type="video/mp4">
        </video>
      </div>
    <?php } ?>

  </div>


<?php } ?>