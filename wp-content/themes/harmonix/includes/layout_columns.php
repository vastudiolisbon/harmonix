<?php
  function render_columns(String $type, Array $data){
    if($type == 'image'){

      $image_url = wp_get_attachment_image_src($data['image'], 'medium_large');
      echo '<img src="'.$image_url[0].'" class="full-img" alt="">';

    }elseif($type == 'video'){

      $video_url = $data['video'];
      echo '<div class="video-wr"><video autoplay muted loop><source src="'.$video_url.'" type="video/mp4"></video></div>' ;

    }else{

      $content = $data['text_content'];
      echo '<div class="no-gutters-padding text-editor">'.$content.'</div>';

    }
  }
?>
