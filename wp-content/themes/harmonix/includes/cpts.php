<?php
/*
	Register Custom Post Type - Portfolio
*/

add_action( 'init', 'create_portfolio_cpt', 0 );

function create_portfolio_cpt() {
  $labels = array(
    'name' => __( 'Companies', 'Post Type General Name', 'harmonix' ),
    'singular_name' => __( 'Companies', 'Post Type Singular Name', 'harmonix' ),
    'menu_name' => __( 'Companies', 'harmonix' ),
    'name_admin_bar' => __( 'Companies', 'harmonix' ),
    'archives' => __( 'Companies Archive', 'harmonix' ),
    'attributes' => __( 'Companies Attrinutes', 'harmonix' ),
    'parent_item_colon' => __( 'Companies Parent:', 'harmonix' ),
    'all_items' => __( 'All Items', 'harmonix' ),
    'add_new_item' => __( 'Add new Company', 'harmonix' ),
    'add_new' => __( 'Add new', 'harmonix' ),
    'new_item' => __( 'new Company Item', 'harmonix' ),
    'edit_item' => __( 'Edit Company', 'harmonix' ),
    'update_item' => __( 'Update Company', 'harmonix' ),
    'view_item' => __( 'View Item', 'harmonix' ),
    'view_items' => __( 'View Items', 'harmonix' ),
    'search_items' => __( 'Search Companies', 'harmonix' ),
    'not_found' => __( 'Not found', 'harmonix' ),
    'not_found_in_trash' => __( 'Not found in trash', 'harmonix' ),
    'featured_image' => __( 'Featured Image', 'harmonix' ),
    'set_featured_image' => __( 'Set Featured Image', 'harmonix' ),
    'remove_featured_image' => __( 'Remove Featured Image', 'harmonix' ),
    'use_featured_image' => __( 'Use as featured image', 'harmonix' ),
    'insert_into_item' => __( 'Insert in Company', 'harmonix' ),
    'uploaded_to_this_item' => __( 'Upload to Company', 'harmonix' ),
    'items_list' => __( 'Companies List', 'harmonix' ),
    'items_list_navigation' => __( 'Companies List Navigation', 'harmonix' ),
    'filter_items_list' => __( 'Filter Companies List', 'harmonix' ),
  );
  $args = array(
    'label' => __( 'Companies', 'harmonix' ),
    'description' => __( '', 'harmonix' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-open-folder',
    'supports' => array('title', 'page-attributes' ),
    'taxonomies' => array(),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'hierarchical' => false,
    'exclude_from_search' => false,
    'show_in_rest' => false,
    'publicly_queryable' => true,
    'capability_type' => 'post',
		'rewrite' => array('slug' => 'companies', 'with_front' => false),
  );
  register_post_type( 'portfolio', $args );
}

/*
	Register Taxonomies
*/

add_action( 'init', 'create_portfolio_tax' );

function create_portfolio_tax() {
	$labels = array(
		'name'              => 'Project Category',
		'singular_name'     => 'Project Category',
		'search_items'      => 'Search Project Categories',
		'all_items'         => 'All categories',
		'parent_item'       => 'Project Category Parent',
		'parent_item_colon' => 'Project Category Parent:',
		'edit_item'         => 'Edit Project Category',
		'update_item'       => 'Update Project Category',
		'add_new_item'      => 'Add new Category',
		'new_item_name'     => 'New Category',
		'menu_name'         => 'Company Categories',
	);

	$args = array(
		'labels' => $labels,
		'description' => __( 'Company Categories', 'harmonix' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => false,
		'show_in_quick_edit' => false,
		'show_admin_column' => true,
		'meta_box_cb' => false,
		'rewrite' => array('slug' => 'company-type', 'with_front' => false)
	);

	register_taxonomy('portfolio-category', array('portfolio'), $args);
}

add_action( 'init', 'create_portfolio_fund' );

function create_portfolio_fund() {
	$labels = array(
		'name'              => 'Project Fund',
		'singular_name'     => 'Project Fund',
		'search_items'      => 'Search Project Funds',
		'all_items'         => 'All Funds',
		'parent_item'       => 'Project Fund Parent',
		'parent_item_colon' => 'Project Fund Parent:',
		'edit_item'         => 'Edit Project Fund',
		'update_item'       => 'Update Project Fund',
		'add_new_item'      => 'Add new Fund',
		'new_item_name'     => 'New Fund',
		'menu_name'         => 'Funds',
	);

	$args = array(
		'labels' => $labels,
		'description' => __( 'Funds', 'harmonix' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => false,
		'show_in_quick_edit' => false,
		'show_admin_column' => true,
		'meta_box_cb' => false,
		'rewrite' => array('slug' => 'company-fund', 'with_front' => false)
	);

	register_taxonomy('portfolio-fund', array('portfolio'), $args);
}

// Custom POst Type - Team

add_action( 'init', 'create_team_cpt', 0 );

function create_team_cpt() {
  $labels = array(
    'name' => __( 'Team', 'Post Type General Name', 'harmonix' ),
    'singular_name' => __( 'Team', 'Post Type Singular Name', 'harmonix' ),
    'menu_name' => __( 'Team', 'harmonix' ),
    'name_admin_bar' => __( 'Team', 'harmonix' ),
    'archives' => __( 'Team Archive', 'harmonix' ),
    'attributes' => __( 'Team Member Attrinutes', 'harmonix' ),
    'parent_item_colon' => __( 'Team Parent:', 'harmonix' ),
    'all_items' => __( 'All Team Members', 'harmonix' ),
    'add_new_item' => __( 'Add new Team Member', 'harmonix' ),
    'add_new' => __( 'Add new Member', 'harmonix' ),
    'new_item' => __( 'New Team Member', 'harmonix' ),
    'edit_item' => __( 'Edit Team Member', 'harmonix' ),
    'update_item' => __( 'Update Team Member', 'harmonix' ),
    'view_item' => __( 'View Team Member', 'harmonix' ),
    'view_items' => __( 'View Team Members', 'harmonix' ),
    'search_items' => __( 'Search Team Member', 'harmonix' ),
    'not_found' => __( 'Not found', 'harmonix' ),
    'not_found_in_trash' => __( 'Not found in trash', 'harmonix' ),
    'featured_image' => __( 'Featured Image', 'harmonix' ),
    'set_featured_image' => __( 'Set Featured Image', 'harmonix' ),
    'remove_featured_image' => __( 'Remove Featured Image', 'harmonix' ),
    'use_featured_image' => __( 'Use as featured image', 'harmonix' ),
    'insert_into_item' => __( 'Insert in Team Member', 'harmonix' ),
    'uploaded_to_this_item' => __( 'Upload to Team Member', 'harmonix' ),
    'items_list' => __( 'Team List', 'harmonix' ),
    'items_list_navigation' => __( 'Team List Navigation', 'harmonix' ),
    'filter_items_list' => __( 'Filter Team List', 'harmonix' ),
  );
  $args = array(
    'label' => __( 'Team', 'harmonix' ),
    'description' => __( 'Harmonix Team', 'harmonix' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-admin-users',
    'supports' => array('title', 'page-attributes' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 6,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'hierarchical' => false,
    'exclude_from_search' => false,
    'show_in_rest' => false,
    'publicly_queryable' => true,
    'capability_type' => 'post',
		'rewrite' => array('slug' => 'team', 'with_front' => false),
  );
  register_post_type( 'team', $args );
}


// Custom Post Type - Job

add_action('init', 'create_job_cpt', 0);

function create_job_cpt()
{
  $labels = array(
    'name' => __('Jobs', 'Post Type General Name', 'harmonix'),
    'singular_name' => __('Job', 'Post Type Singular Name', 'harmonix'),
    'menu_name' => __('Jobs', 'harmonix'),
    'name_admin_bar' => __('Job', 'harmonix'),
    'archives' => __('Job Archive', 'harmonix'),
    'attributes' => __('Job Attrinutes', 'harmonix'),
    'parent_item_colon' => __('Job Parent:', 'harmonix'),
    'all_items' => __('All Jobs', 'harmonix'),
    'add_new_item' => __('Add new Job ', 'harmonix'),
    'add_new' => __('Add new Job', 'harmonix'),
    'new_item' => __('New Job ', 'harmonix'),
    'edit_item' => __('Edit Job ', 'harmonix'),
    'update_item' => __('Update Job ', 'harmonix'),
    'view_item' => __('View Job ', 'harmonix'),
    'view_items' => __('View Job s', 'harmonix'),
    'search_items' => __('Search Job ', 'harmonix'),
    'not_found' => __('Not found', 'harmonix'),
    'not_found_in_trash' => __('Not found in trash', 'harmonix'),
    'featured_image' => __('Featured Image', 'harmonix'),
    'set_featured_image' => __('Set Featured Image', 'harmonix'),
    'remove_featured_image' => __('Remove Featured Image', 'harmonix'),
    'use_featured_image' => __('Use as featured image', 'harmonix'),
    'insert_into_item' => __('Insert in Job ', 'harmonix'),
    'uploaded_to_this_item' => __('Upload to Job ', 'harmonix'),
    'items_list' => __('Job List', 'harmonix'),
    'items_list_navigation' => __('Job List Navigation', 'harmonix'),
    'filter_items_list' => __('Filter Job List', 'harmonix'),
  );
  $args = array(
    'label' => __('Jobs', 'harmonix'),
    'description' => __('Harmonix Jobs', 'harmonix'),
    'labels' => $labels,
    'menu_icon' => 'dashicons-id',
    'supports' => array('title', 'page-attributes'),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 6,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'hierarchical' => false,
    'exclude_from_search' => false,
    'show_in_rest' => false,
    'publicly_queryable' => true,
    'capability_type' => 'post',
    // 'rewrite' => array('slug' => 'team', 'with_front' => false),
  );
  register_post_type('jobs', $args);
}

add_action('init', 'create_jobs_tax');

function create_jobs_tax()
{
  $labels = array(
    'name'              => 'Job Category',
    'singular_name'     => 'Job Category',
    'search_items'      => 'Search Job Categorys',
    'all_items'         => 'All Categories',
    'parent_item'       => 'Job Category Parent',
    'parent_item_colon' => 'Job Category Parent:',
    'edit_item'         => 'Edit Job Category',
    'update_item'       => 'Update Job Category',
    'add_new_item'      => 'Add new Category',
    'new_item_name'     => 'New Category',
    'menu_name'         => 'Categories',
  );

  $args = array(
    'labels' => $labels,
    'description' => __('Job Catgeories', 'harmonix'),
    'hierarchical' => false,
    'public' => false,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_rest' => false,
    'show_tagcloud' => false,
    'show_in_quick_edit' => false,
    'show_admin_column' => true,
    'meta_box_cb' => false,
    // 'rewrite' => array('slug' => 'company-fund', 'with_front' => false)
  );

  register_taxonomy('job-category', array('jobs'), $args);
}

 ?>
