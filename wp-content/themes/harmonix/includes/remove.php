<?php

/* Disable the emoji's  */

function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Remove from TinyMCE
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter out the tinymce emoji plugin.
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/* Disable Links to Image URL */
add_action( 'after_setup_theme', 'default_attachment_display_settings' );
function default_attachment_display_settings() {
	update_option( 'image_default_link_type', 'none' );
}


/* Disable xmlrpc */

add_filter( 'xmlrpc_enabled', '__return_false' );

// Hide xmlrpc.php in HTTP response headers
add_filter( 'wp_headers', function( $headers ) {
    unset( $headers[ 'X-Pingback' ] );
    return $headers;
} );
remove_action('wp_head', 'wlwmanifest_link');

/* Disable REST API */
add_filter( 'rest_authentication_errors', function( $result ) {
  if ( ! empty( $result ) ) {
    return $result;
  }
//   if ( ! is_user_logged_in() ) {
//     return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
//   }
  return $result;
});

/* Disable RSS */
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'rest_output_link_wp_head');
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action( 'template_redirect', 'rest_output_link_header', 11 );


/* Remove Gutenberg Block Library CSS from loading on the frontend */

function smartwp_remove_wp_block_library_css(){
 wp_dequeue_style( 'wp-block-library' );
 wp_dequeue_style( 'wp-block-library-theme' );
}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css' );

function disable_gutenberg_for_specific_post_types($can_edit, $post_type) {
    if ($post_type === 'page') {
        return false;
    }
    return false;
}
add_filter('use_block_editor_for_post_type', 'disable_gutenberg_for_specific_post_types', 10, 2);

function enable_gutenberg_for_specific_post_types($can_edit, $post_type) {
    if ($post_type === 'post') {
        return true;
    }
    return false;
}
add_filter('use_block_editor_for_post_type', 'enable_gutenberg_for_specific_post_types', 10, 2);


?>
