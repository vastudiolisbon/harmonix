<?php

/* Replace Login Logo */

function my_login_logo() { ?>
	<style type="text/css">
	#login h1 a, .login h1 a {
		background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/harmonix-logo-blue.svg);
		height:65px;
		width:320px;
		background-size: auto 65px;
		background-repeat: no-repeat;
		padding-bottom: 10px;
	}
	#login form {
		background: transparent;
		border: none;
		box-shadow: none;
	}
	#login form input:focus {
		border-color: black;
		box-shadow: 0 0 0 1px black;
	}
	#login .button-primary {
		background: darkgray;
		border-color: darkgray;
	}
	#login .button-primary:hover {
		background: black;
		border-color: black;
	}
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function wpb_login_logo_url() {
return home_url();
}
add_filter( 'login_headerurl', 'wpb_login_logo_url' );

function wpb_login_logo_url_title() {
return 'Hamornix';
}

add_filter('tiny_mce_before_init', 'tinymce_paste_as_text');
function tinymce_paste_as_text( $init ) {
  $init['paste_as_text'] = true;

  //$init["toolbar2"] = "formatselect,underline,alignjustify,forecolor,removeformat,charmap,outdent,indent,undo,redo,wp_help";
  $init['block_formats'] = 'Paragraph=p;Title=h2;';

  return $init;
}

add_filter( 'mce_buttons_2', 'add_style_select_buttons' );
function add_style_select_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
function my_mce_before_init_insert_formats( $init_array ) {
  $style_formats = array(
    array(
    	'title' => 'Title',
    	'block' => 'h2',
    	'classes' => 'h2',
    	'exact' => true
    ),
    array(
    	'title' => 'Title 3',
    	'block' => 'h3',
    	'classes' => 'h3',
    	'exact' => true
    ),
	array(
		'title' => 'Title 4',
		'block' => 'h3',
		'classes' => 'h6',
		'exact' => true
	),
    array(
    	'title' => 'Pill Button',
    	'inline' => 'a',
    	'classes' => 'pill',
    	'exact' => false
    ),
  );

  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;
}


/* Hide Login Errors */

function no_wordpress_errors(){
  return 'Something is wrong!';
}
add_filter( 'login_errors', 'no_wordpress_errors' );

/* Customize Admin Bar */
function remove_from_admin_bar($wp_admin_bar) {

    $wp_admin_bar->remove_node('comments');
    $wp_admin_bar->remove_node('search');
    $wp_admin_bar->remove_node('customize');
    $wp_admin_bar->remove_node('wp-logo');
}
add_action('admin_bar_menu', 'remove_from_admin_bar', 999);


function wpdocs_custom_excerpt_length($length)
{
	return 20;
}
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);

function my_special_nav_class($classes, $item)
{
	
	if ($item->object_id == 569 && is_singular('post')) {
		$classes[] = 'current-menu-item';
	}elseif($item->object === 'jobs' && is_singular('jobs')){
		$classes[] = 'current-menu-item';
	}

	return $classes;
}
add_filter('nav_menu_css_class', 'my_special_nav_class', 99, 2);


 ?>
