<?php

/* Register Main Menu */

add_action( 'init', 'register_menus' );
function register_menus() {
	register_nav_menu('main-menu', 'Main Menu');
	register_nav_menu('footer-menu', 'Footer Menu');
}


/* Custom Output MenuWalker */
//
//  class mainMenuWalker extends Walker_Nav_Menu {
//
// 	function start_lvl( &$output, $depth = 0, $args = array() ) {
// 		$output .= "";
// 	}
//
// 	function end_lvl( &$output, $depth = 0, $args = array() ) {
// 		$output .= "</div></div>";
// 	}
//
//   function start_el(&$output, $item, $depth = 0, $args=array(), $id = 0) {
//   	$object = $item->object;
//   	$type = $item->type;
//   	$title = $item->title;
// 		$id = remove_accents($title);
// 		$id = sanitize_title_with_dashes($id);
//   	$description = $item->description;
//   	$permalink = $item->url;
// 		$indent = str_repeat("\t", $depth);
//
// 		if($args->walker->has_children) :
//       $output .= '<li class="dropdown">
// 			<button class="btn btn-secondary dropdown-toggle" type="button" id="'.$id.'-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
// 			'. $title .'</button>';
// 			$output .= "\n$indent<div class=\"dropdown-menu\" aria-labelledby=\"".$id."-btn\"><div class=\"container\">\n";
//     elseif($item->menu_item_parent != 0):
// 	    $output .= '<a class="dropdown-item" href="' . $permalink . '">';
// 	    $output .= $title;
// 		elseif(!$args->walker->has_children):
// 			$output .= "<li class=''>";
// 			$output .= '<a href="' . $permalink . '">';
// 			$output .= $title;
// 		else:
// 			$output .= "<li class=''>";
// 			$output .= $title;
// 		endif;
//   }
//
// 	function end_el(&$output, $item, $depth = 0, $args=array(), $id = 0) {
// 		$object = $item->object;
//   	$type = $item->type;
//   	$title = $item->title;
//   	$description = $item->description;
//   	$permalink = $item->url;
// 		if($args->walker->has_children) :
//       $output .= '</li></div>el-menu</div>';
//     elseif($item->menu_item_parent != 0):
// 	    	$output .= '</a>';
// 		elseif($type == 'custom'):
// 			$output .= '</li>';
// 		else:
// 			$output .= '</a></li>';
// 		endif;
// 	}
// }
//
// /* Add Search to menu-items */
//
// add_filter( 'wp_nav_menu_items', 'add_search_to_nav', 10, 2 );
//
// function add_search_to_nav( $items, $args )
// {
// 		if($args->walker):
//
// 	    $items .= '<li class="dropdown search">
// 	        <button class="btn btn-secondary dropdown-toggle" type="button" id="search-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Pesquisar">Pesquisar</button>
// 	        <div class="dropdown-menu" aria-labelledby="search-btn" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-5px, 80px, 0px);">
// 	          <form class="container" id="searchform" role="search" action="'. site_url() .'" method="get">
// 							<label class="sr-only" for="srch-input">Procurar</label>
// 	            <input id="srch-input" type="search" name="s" value="" class="dropdown-item" placeholder="Escreva um termo a procurar" aria-label="Escreva um termo a procurar">
// 	            <button type="submit" class="dropdown-item" aria-label="Pesquisar">Pesquisar</button>
// 	          </form>
// 	        </div>
// 	      </li>';
// 			else:
// 				$items = $items;
// 			endif;
//
//     return $items;
// }


?>
