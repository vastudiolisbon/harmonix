<?php

add_action( 'init', function() {
	register_taxonomy( 'entry-stage', array(
	0 => 'portfolio',
), array(
	'labels' => array(
		'name' => 'Entry Stages',
		'singular_name' => 'Entry Stage',
		'menu_name' => 'Entry Stage',
		'all_items' => 'All Entry Stage',
		'edit_item' => 'Edit Entry Stage',
		'view_item' => 'View Entry Stage',
		'update_item' => 'Update Entry Stage',
		'add_new_item' => 'Add New Entry Stage',
		'new_item_name' => 'New Entry Stage Name',
		'search_items' => 'Search Entry Stage',
		'popular_items' => 'Popular Entry Stage',
		'separate_items_with_commas' => 'Separate entry stage with commas',
		'add_or_remove_items' => 'Add or remove entry stage',
		'choose_from_most_used' => 'Choose from the most used entry stage',
		'not_found' => 'No entry stage found',
		'no_terms' => 'No entry stage',
		'items_list_navigation' => 'Entry Stage list navigation',
		'items_list' => 'Entry Stage list',
		'back_to_items' => '← Go to entry stage',
		'item_link' => 'Entry Stage Link',
		'item_link_description' => 'A link to a entry stage',
	),
	'public' => true,
	'show_in_menu' => true,
	'show_in_rest' => true,
	'show_tagcloud' => false,
	'show_admin_column' => true,
	'meta_box_cb' => false,
	'sort' => true,
) );

	register_taxonomy( 'company_status', array(
	0 => 'portfolio',
), array(
	'labels' => array(
		'name' => 'Status',
		'singular_name' => 'Status',
		'menu_name' => 'Status',
		'all_items' => 'All Status',
		'edit_item' => 'Edit Status',
		'view_item' => 'View Status',
		'update_item' => 'Update Status',
		'add_new_item' => 'Add New Status',
		'new_item_name' => 'New Status Name',
		'search_items' => 'Search Status',
		'popular_items' => 'Popular Status',
		'separate_items_with_commas' => 'Separate status with commas',
		'add_or_remove_items' => 'Add or remove status',
		'choose_from_most_used' => 'Choose from the most used status',
		'not_found' => 'No status found',
		'no_terms' => 'No status',
		'items_list_navigation' => 'Status list navigation',
		'items_list' => 'Status list',
		'back_to_items' => '← Go to status',
		'item_link' => 'Status Link',
		'item_link_description' => 'A link to a status',
	),
	'public' => true,
	'show_in_menu' => true,
	'show_in_rest' => true,
	'show_tagcloud' => false,
	'show_admin_column' => true,
	'meta_box_cb' => false,
	'sort' => true,
) );
} );



?>