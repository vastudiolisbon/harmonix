<?php

  include 'parts/includes/header.php';

  $team_members = get_posts([
    'post_type' => 'team',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'orderby' => 'menu_order',
    'order' => 'ASC',
  ]);

  
?>

<div class="grid-page-container dark">
  <div class="container-fluid">
    <div class="row">
      <div class="swiper-container team-swiper">
        <div class="swiper-wrapper">
            <?php
              foreach ($team_members as $key => $member){

                $img_id = get_field('photo', $member->ID);
                $img = wp_get_attachment_image_src($img_id, 'medium_large');
            ?>
                <div class="swiper-slide" <?php if($post->ID === $member->ID): echo 'data-active="1"'; endif; ?>>
                  <div class="img-wrapper">
                    <img src="<?php echo $img[0]; ?>" alt="<?php echo get_the_title($member->ID); ?>">
                  </div>
                  <div class="team-member-info">
                    <h2  class="h3"><?php echo get_the_title($member->ID); ?></h2>
                    <p><?php echo get_field('position', $member->ID); ?></p>
                    <p><?php echo get_field('biography', $member->ID); ?></p>
                    <ul class="team-social">
                      <?php $social_profiles = get_field('social_profiles', $member->ID); ?>
                      <?php
                        if(!empty($social_profiles)){
                          foreach ($social_profiles as $key => $profile) { ?>
                          <li><a href="<?php echo $profile['link']; ?>" target="_blank" rel="noopener"><?php echo $profile['link_text']; ?></a></li>
                        <?php } } ?>
                    </ul>
                  </div>


                </div>
            <?php
              }
            ?>
        </div>

      </div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div>
    </div>
  </div>
</div>
<?php include 'parts/includes/footer.php'; ?>
