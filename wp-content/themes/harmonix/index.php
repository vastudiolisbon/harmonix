<?php

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly
}

/* Template name: Homepage */ ?>

<?php

include 'parts/includes/header.php';

$videos = get_field('videos', $post->ID);
if (!empty($videos['loader_image'])) {
  $preloader_image = wp_get_attachment_image_src($videos['loader_image'], 'full')[0];
} else {
  $preloader_image = get_template_directory_uri() . 'assets/img/poster.jpg';
}

$background_video = $videos['background_video'];

?>

<div id="preloader" style="background-image: url(<?php echo $preloader_image; ?>)">
  <img src="<?php echo get_template_directory_uri() . '/assets/img/harmonix-logo-w.svg'; ?>" alt="harmonix_logo">
</div>
<section id="home-hero">
  <div class="video-wrapper">
    <video autoplay muted loop playsinline>
      <source src="<?php echo $background_video; ?>" type="video/mp4">
    </video>
  </div>
  <div class="hero-content">
    <div id="lottie"></div>
    <div class="quote">
      <div class="text-block">
        Pioneering the Future of Health
      </div>
    </div>
  </div>
  <a href="#content" class="arrow-down">
    <img src="<?php echo get_template_directory_uri() . '/assets/img/arrow.svg'; ?>" alt="arrow">
  </a>
  <button type="button" class="play-btn pill pill-light" data-toggle="modal" data-target=".home-video">Play video</button>
  <div class="modal fade home-video" tabindex="-1" role="dialog" aria-labelledby="home-video" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">
          <svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M37.5 1L1 37.5M1 1L37.5 37.5" stroke="white"/>
          </svg>
        </span>
    </button>
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="plyr__video-embed player">
          <iframe
          src="https://player.vimeo.com/video/915743661?h=d526fad880&title=0&byline=0&portrait=0"
          allowfullscreen
          allowtransparency
          allow="autoplay"
        ></iframe>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="bg-lighter">
  <?php include 'parts/content-layout.php'; ?>
</div>


<?php
  include 'parts/homepage-graph.php';
  include 'parts/homepage-portfolio.php';
  include 'parts/homepage-pipeline.php';
  include 'parts/includes/footer.php';
?>
