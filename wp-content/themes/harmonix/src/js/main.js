global.$ = require("jquery");
require("popper.js");
require("bootstrap");

const ObjectFitImages = require("object-fit-images");
const Swiper = require("swiper");
const Isotope = require("isotope-layout");

import lottie from "lottie-web";
import Cookies from "js-cookie";

var mobile_w = 767;
var $preloader, $title;

$(document).ready(function () {
  $preloader = $("#preloader");
  $title = $preloader.find("img");

  if ($preloader.length && !Cookies.get("preloader")) {
    $("body, html").addClass("no-scroll");
    $(window).scrollTop(0);
  }

  const vimeoPlayer = new Vimeo.Player(document.querySelector('.player'));
  $('.home-video').on('hidden.bs.modal', function () {
      vimeoPlayer.pause();
  });
  
});


$(window).on("load", function () {
  
  if ($preloader.length && !Cookies.get("preloader")) {
    setTimeout(function () {
      $title.fadeOut(600, function () {
        $preloader.fadeOut(600, function () {
          anim.play();
          $("body, html").removeClass("no-scroll");
          Cookies.set("preloader", "seen", { expires: 0 });
        });
      });
    }, 800);
  }

  /*
        ajaxMailChimpForm($("#subscribe-form"), $("#subscribe-result"));
        ajaxMailChimpForm($("#subscribe-form-2"), $("#subscribe-result-2"));

        function ajaxMailChimpForm($form, $resultElement) {
            // Hijack the submission. We'll submit the form manually.
            $form.submit(function (e) {
                e.preventDefault();
                if (!isValidEmail($form)) {
                    var error = "A valid email address must be provided.";
                    $resultElement.html(error);
                    $resultElement.css("color", "red");
                } else {
                    $resultElement.css("color", "white");
                    $resultElement.html("Subscribing...");
                    submitSubscribeForm($form, $resultElement);
                }
            });
        }
        // Validate the email address in the form
        function isValidEmail($form) {
            // If email is empty, show error message.
            // contains just one @
            var email = $form.find("input[type='email']").val();
            if (!email || !email.length) {
                return false;
            } else if (email.indexOf("@") == -1) {
                return false;
            }
            return true;
        }
        // Submit the form with an ajax/jsonp request.
        // Based on http://stackoverflow.com/a/15120409/215821
        function submitSubscribeForm($form, $resultElement) {
            $.ajax({
                type: "GET",
                url: $form.attr("action"),
                data: $form.serialize(),
                cache: false,
                dataType: "jsonp",
                jsonp: "c", // trigger MailChimp to return a JSONP response
                contentType: "application/json; charset=utf-8",
                error: function (error) {
                    // According to jquery docs, this is never called for cross-domain JSONP requests
                },
                success: function (data) {
                    if (data.result != "success") {
                        var message = data.msg || "Sorry. Unable to subscribe. Please try again.";
                        $resultElement.css("color", "red");
                        if (data.msg && data.msg.indexOf("already subscribed") >= 0) {
                            message = "You're already subscribed. Thank you.";
                            $resultElement.css("color", "#00ccbd");
                        }
                        $resultElement.html(message);
                    } else {
                        $resultElement.css("color", "#00ccbd");
                        $resultElement.html("Thank you! You must confirm the subscription in your inbox.");
                    }
                }
            });
        }
        */
  var activeIndex = $('*[data-active="1"]').index();

  var teamSwiper = new Swiper(".swiper-container", {
    speed: 400,
    spaceBetween: 10,
    centeredSlides: true,
    autoHeight: true,
    loop: true,
    initialSlide: activeIndex,
    slidesPerView: 2,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      // when window width is >= 320px
      768: {
        slidesPerView: 3,
        spaceBetween: 50,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 62,
      },
    },
  });

  var $document = $(document),
    $element = $(".site-nav"),
    className = "hasScrolled";

  $document.scroll(function () {
    if ($document.scrollTop() >= 80) {
      // user scrolled 50 pixels or more;
      // do stuff
      $element.addClass(className);
    } else {
      $element.removeClass(className);
    }
    if (
      $document.scrollTop() > window.innerHeight &&
      !Cookies.get("popupClosed")
    ) {
      setTimeout(function () {
        $(".popup").addClass("active");
      }, 500);
    }
  });

  $(".popup-close").click(function (e) {
    $(".popup").removeClass("active");
    Cookies.set("popupClosed", "yes", { expires: 7 });
  });

  ObjectFitImages();

  $("a[href='#content']").click(function () {
    var id = $(this).attr("href");
    $("html, body").animate(
      {
        scrollTop: $(id).offset().top,
      },
      1000
    );
  });

  // Nav
  $("#menu-toggle").click(function (e) {
    if ($("#menu-toggle").is(":checked")) {
      $(".site-nav").addClass("mobile-nav-active");
    } else {
      $(".site-nav").removeClass("mobile-nav-active");
    }
  });

  var params = {
    container: document.getElementById("lottie"),
    renderer: "svg",
    loop: false,
    autoplay: false,
    path: "wp-content/themes/harmonix/assets/Ani_White_001.json",
  };

  var anim;
  anim = lottie.loadAnimation(params);

  // Portfolio
  if ($(".portfolio-items-container").length) {
    var elem = document.querySelector(".portfolio-items-container");
    var iso = new Isotope(elem, {
      itemSelector: ".portfolio-item, .portfolio-item-heading",
      layoutMode: "fitRows",
      hiddenStyle: {
        opacity: 0,
      },
      visibleStyle: {
        opacity: 1,
      },
    });

    setTimeout(function () {
      $(".portfolio-filter li.active").trigger("click");
    }, 100);

    const filterButtons = document.querySelectorAll(".portfolio-filter li");
    const filterPills = document.querySelectorAll(
      ".tax-pills-wrapper .filter-pill"
    );
    const clearFilters = document.querySelector(".clear-filters");

    iso.on("arrangeComplete", function (filteredItems) {
      $(".portfolio-items-container").addClass("visible");

      $(".category-container").each(function () {
        var $section = $(this);
        var numItems = $section.find(".portfolio-item:visible").length;
        if (numItems == 0) {
          $section.find(".portfolio-item-heading").addClass("hidden");
        }
      });
    });

    var activeFilters = {};

    var selectedFund = "";

    filterButtons.forEach(function (button) {
      button.addEventListener("click", function () {
        var filterGroup = button.getAttribute("data-filter-group");
        var filterValue = button.getAttribute("data-filter");

        // Remove "active" class from all buttons in the same group
        filterButtons.forEach(function (btn) {
          if (btn.getAttribute("data-filter-group") === filterGroup) {
            btn.classList.remove("active");
          }
        });

        button.classList.add("active");

        activeFilters[filterGroup] = [filterValue];

        var combinations = generateCombinations(Object.values(activeFilters));

        var combinedFilterValue = combinations
          .map(function (combo) {
            return combo.join("");
          })
          .join(", ");

        selectedFund = filterValue;

        console.log(activeFilters);
        console.log(selectedFund);

        iso.arrange({ filter: combinedFilterValue });
      });
    });

    filterPills.forEach(function (button) {
      button.addEventListener("click", function () {
        button.classList.toggle("active");
        $(".clear-filters").addClass("active");

        var filterGroup = button
          .closest(".portfolio-filter-wrapper")
          .getAttribute("data-filter-group");

        if (!activeFilters[filterGroup]) {
          activeFilters[filterGroup] = [];
        }

        if (button.classList.contains("active")) {
          activeFilters[filterGroup].push(button.getAttribute("data-filter"));
        } else {
          var index = activeFilters[filterGroup].indexOf(
            button.getAttribute("data-filter")
          );
          if (index !== -1) {
            activeFilters[filterGroup].splice(index, 1);
            activeFilters[filterGroup].push("");
          }
          if (activeFilters[filterGroup].length === 0) {
            delete activeFilters[filterGroup];
          }
        }

        var combinations = generateCombinations(Object.values(activeFilters));

        var filterValue = combinations
          .map(function (combo) {
            if (combo.length > 0) {
              return combo.join("");
            } else {
              return "*";
            }
          })
          .join(", ");

        console.log(activeFilters);
        console.log(filterValue);

        iso.arrange({ filter: filterValue });
      });
    });

    function generateCombinations(arrays, index, current) {
      index = index || 0;
      current = current || [];
      if (index === arrays.length) {
        return [current];
      }
      var result = [];
      for (var i = 0; i < arrays[index].length; i++) {
        result = result.concat(
          generateCombinations(
            arrays,
            index + 1,
            current.concat(arrays[index][i])
          )
        );
      }

      return result;
    }

    iso.on("arrangeComplete", function (filteredItems) {
      $(".portfolio-items-container").addClass("visible");

      $(".category-container").each(function () {
        var $section = $(this);
        var numItems = $section.find(".portfolio-item:visible").length;
        if (numItems == 0) {
          $section.find(".portfolio-item-heading").addClass("hidden");
        }
      });
    });

    $(".jobs-page .portfolio-filter li:not(.filter-all)").click(function (e) {
      $(".portfolio-items-container.visible").addClass("job-cat");
    });

    if (clearFilters) {
      clearFilters.addEventListener("click", function () {
        filterPills.forEach(function (pill) {
          pill.classList.remove("active");
        });
        $(this).removeClass("active");
        iso.arrange({ filter: selectedFund });
        activeFilters = {};
      });
    }

    $(".portfolio-item").click(function (e) {
      if ($(e.target).hasClass("popup-close")) return;
      $(".portfolio-item").removeClass("active info-right");
      $(this).addClass("active");
      console.log($(this).offset().left);
      console.log((window.innerWidth / 4) * 3);
      if ($(this).offset().left >= (window.innerWidth / 4) * 3 - 3) {
        $(this).addClass("info-right");
      }
    });

    $(".portfolio-item .popup-close").click(function (e) {
      $(".portfolio-item").removeClass("active");
    });
  }
});
