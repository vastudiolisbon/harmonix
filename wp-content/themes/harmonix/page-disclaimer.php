<?php

/* Template name: Disclaimer Page */

include 'parts/includes/header.php'; ?>

  <div class="container-fluid py-5 dark">
    <div class="row">
      <div class="col-lg-8 offset-lg-2 pb-5">
        <h2><?php echo get_the_title($post->ID); ?></h2>
        <p>
          <?php echo the_content(); ?>
        </p>
      </div>
    </div>
  </div>

<?php
include 'parts/includes/footer.php';
?>
