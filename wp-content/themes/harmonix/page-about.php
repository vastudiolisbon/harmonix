<?php

/* Template name: About Page */

 include 'parts/includes/header.php';

 $cover = get_field('cover', $post->ID);
 ?>
  <section class="hero-section">
    <div class="container-fluid">
      <div class="row no-gutters">
        <div class="hero-column col-md-6 bg-light">
          <div class="no-gutters-padding text-editor">
            <h1 class="h2"><?php echo get_the_title($post->ID); ?></h1>
            <p><?php echo get_field('description', $post->ID); ?></p>
          </div>
        </div>
        <div class="hero-column col-md-6">
          <?php
            if ($cover['media_type'] == 'image') {
              $img_id = $cover['image'];
              $img_src = wp_get_attachment_image_src($img_id, 'medium_large');
          ?>
              <img src="<?php echo $img_src; ?>" alt="<?php get_the_title($post->ID); ?>">
          <?php
            }elseif($cover['media_type'] == 'video'){
              $video_url = $cover['video_url'];
              echo '<div class="video-wr"><video autoplay muted loop><source src="'.$video_url.'" type="video/mp4"></video></div>' ;
          ?>
           
        <?php } ?>
        </div>
      </div>
    </div>
  </section>


<?php
include 'parts/content-layout.php';
include 'parts/includes/footer.php';
?>
