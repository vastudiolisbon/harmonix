<?php
include 'parts/includes/header.php';

$categories = get_terms([
  'taxonomy' => 'portfolio-category',
  'meta_key' => 'tax_position',
  'orderby' => 'tax_position',
  'hide_empty' => true
]);

$tax = false;
?>

<div class="grid-page-container dark">
  <div class="container-fluid dark">
    <h1>Harmonix Companies</h1>
    <div id="output"></div>
    <div class="row">
      <div class="col-12">
      <?php include 'parts/portfolio-filter.php'; ?>

        <div class="row portfolio-pill-filters">
          <div class="col-auto portfolio-filter-wrapper" data-filter-group="0">
            <p>Entry Stage</p>
            <?php
              $taxonomy = 'entry-stage';
              $args = array(
                  'hide_empty' => true,
              );
              $terms = get_terms($taxonomy, $args);
              if (!empty($terms) && !is_wp_error($terms)) {
                echo '<div class="tax-pills-wrapper">';
                  foreach ($terms as $term) {
                      echo '<span class="filter-pill button" data-filter=".entry-' .$term->slug. '">' . $term->name . '</span>';
                  }
                  echo '</div>';
              }
            ?>
          </div>
          <div class="col-auto portfolio-filter-wrapper" data-filter-group="1">
            <p>Status</p>
            <?php
              $taxonomy = 'company_status';
              $args = array(
                  'hide_empty' => true,
              );
              $terms = get_terms($taxonomy, $args);
              if (!empty($terms) && !is_wp_error($terms)) {
                echo '<div class="tax-pills-wrapper">';
                  foreach ($terms as $term) {
                    echo '<span class="filter-pill button" data-filter=".status-' .$term->slug. '">' . $term->name . '</span>';
                  }
                  echo '</div>';
              }
            ?>
          </div>
          <div class="col-auto portfolio-filter-wrapper" data-filter-group="2">
            <p>Sector</p>
            <?php
              $taxonomy = 'portfolio-category';
              $args = array(
                  'hide_empty' => true,
              );
              $terms = get_terms($taxonomy, $args);
              if (!empty($terms) && !is_wp_error($terms)) {
                echo '<div class="tax-pills-wrapper">';
                  foreach ($terms as $term) {
                    echo '<span class="filter-pill button" data-filter=".type-' .$term->slug. '">' . $term->name . '</span>';
                  }
                  echo '</div>';
              }
            ?>
          </div>
          <!-- <div class="col-auto ml-auto">
            <p>&nbsp;</p>
            <span class="clear-filters button"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="1" stroke-linecap="round" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> Clear all</span>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <div class="portfolio-items-container">
    <?php
    $cats = get_terms(['taxonomy' => 'portfolio-fund', 'hide_empty' => true, 'meta_key' => 'tax_position', 'orderby' => 'tax_position', ]); ?>
   
    <?php
      $args = array(
      'offset' => 0,
      'posts_per_page' => -1,
      'orderby' => 'date',
      'order' => 'DESC',
      'post_type' => 'portfolio',
    );
      $posts = new WP_Query( $args );

      if ( $posts->have_posts() ) :

      while ( $posts->have_posts() ) : $posts->the_post();

      get_template_part( 'parts/portfolio-card' );

      endwhile;

      endif;
      wp_reset_postdata();
    ?>

  </div>

</div>
</div>


<?php include 'parts/includes/footer.php'; ?>
