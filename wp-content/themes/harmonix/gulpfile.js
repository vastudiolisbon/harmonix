//utils
const gulp = require('gulp');
const gutil = require('gulp-util');
const rename = require('gulp-rename');

//css
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const autoprefixer = require('autoprefixer');

//babel + browserify
const babel = require('gulp-babel');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const uglify = require('gulp-uglify');


gulp.task('default', ['sass', 'babel'], () => {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['babel']);
});

gulp.task('sass', () => {
    const plugins = [autoprefixer({ browsers: ['last 2 versions'] }), cssnano( {zindex: false})];
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(plugins))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/css'));
});


gulp.task('babel', function() {
    browserify({
        entries: './src/js/main.js',
        debug: false
    })
    .transform(babelify, { presets: ['env','react'] })
    .on('error',gutil.log)
    .bundle()
    .on('error',gutil.log)
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('dist/js'));
});


gulp.task('scripts', function() {
  return gulp.src('./dist/js/bundle.js')
    // Minify the file
    .pipe(uglify())
    .pipe(rename({ suffix: ".min"}))
    // Output
    .pipe(gulp.dest('./dist/js'))
});
