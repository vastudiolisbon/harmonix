<?php
/* Template name: Testimonials Page */
include 'parts/includes/header.php';
?>


  
  <div class="grid-page-container bg-dark testimonials-page">
    
    <div class="container-fluid bg-dark">
      <h1 class="page-title">Testimonials</h1>
    </div>

  <?php if( have_rows('testimonials') ): ?>
    <div class="portfolio-items-container">
    <?php while( have_rows('testimonials') ): the_row(); 
        $photo = get_sub_field('photo');
        $name = get_sub_field('name');
        $role = get_sub_field('role');
        $intro_text = get_sub_field('intro_text');
        $quote = get_sub_field('quote');
        ?>
        
  <div class="portfolio-item">
    <div class="col-12 px-0">
      <div class="d-flex">
        <div class="picture"><img class="profile-picture" src="<?php echo $photo['sizes']['thumbnail']?>"></div>
        <div class="person-meta ml-3">
          <p><?php echo $name;?><br>
            <span class="role"><?php echo $role;?></span></p>
        </div>
      </div>
      <div class="quote-intro">
        <h5><?php echo $intro_text;?></h5>
      </div>

    </div>

    <div class="portfolio-expandable">
    <div class="col-12 px-0">
      <div class="d-flex">
        <div class="picture"><img class="profile-picture" src="<?php echo $photo['sizes']['thumbnail']?>"></div>
        <div class="person-meta ml-3">
          <p><?php echo $name;?><br>
            <span class="role"><?php echo $role;?></span></p>
        </div>
      </div>
      <div class="quote-intro">
        <h5><?php echo $quote;?></h5>
      </div>

    </div>
      
      <button class="popup-close"></button>
    </div>
  </div>


    <?php endwhile; ?>
    </div>
  <?php endif; ?>

    
  </div>




<?php include 'parts/includes/footer.php'; ?>
