<?php
include 'parts/includes/header.php';

$categories = get_terms([
  'taxonomy' => 'job-category',
  'meta_key' => 'tax_position',
  'orderby' => 'tax_position',
  'hide_empty' => true
]);

$tax = false;
?>

<div class="grid-page-container dark">
  <div class="container-fluid dark jobs-page">
    <h1>Harmonix</h1>
    <div class="row">
      <div class="col-12">
        <?php include 'parts/jobs-filter.php'; ?>
      </div>
    </div>
  </div>
  <div class="portfolio-items-container">
    <?php
    $cats = get_terms(['taxonomy' => 'job-category', 'hide_empty' => true, 'meta_key' => 'tax_position', 'orderby' => 'tax_position', ]); ?>

    <?php
          foreach ($categories as $key => $category) {

            $jobs = get_posts([
              'post_type' => 'jobs',
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'orderby' => 'menu_order',
              'order' => 'DESC',
              'tax_query' => array(
                'relation' => 'AND',
                array(
                  'taxonomy' => 'job-category',
                  'field'    => 'slug',
                  'terms'    => $category->slug
                ),
              )
            ]);


          if($category->slug != 'uncategorized' && !empty($jobs)){
            echo "<div class='category-container'>";
            include 'parts/companies-archive-categories.php';
          }

          include 'parts/job-card.php';

          if($category->slug != 'uncategorized' && !empty($jobs)){
            echo "</div>";
          }
        } ?>


  </div>

</div>
</div>


<?php include 'parts/includes/footer.php'; ?>
