<?php

include 'parts/includes/header.php';  ?>
 <div class="container-fluid">
    <div class="row no-gutters">


<?php
  $team_members = get_posts([
    'post_type' => 'team',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'orderby' => 'menu_order',
    'order' => 'ASC',
  ]);

  foreach ($team_members as $member) {
    $image_url = wp_get_attachment_image_src(get_field('photo', $member->ID), 'medium_large');
    ?>

    <div class="team-member col-md-6 col-lg-4 ">
      <a href="<?php echo get_the_permalink($member->ID); ?>">
        <div class="details">
          <h3><?php echo get_the_title($member->ID); ?></h3>
          <p><?php echo get_field('position', $member->ID); ?></p>
        </div>
        <img src="<?php echo $image_url[0]; ?>" alt="<?php echo get_the_title($member->ID); ?>">
      </a>
    </div>

  <?php } ?>

    </div>
  </div>
<?php include 'parts/includes/footer.php'; ?>
