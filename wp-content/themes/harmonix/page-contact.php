<?php /* Template name: Contact Page */ ?>

<?php include 'parts/includes/header.php'; ?>


  <div class="row no-gutters" id="contact">
    <div class="col-lg-6 contact-bg">
      <div class="container d-flex flex-column justify-content-between">
      <?php echo get_the_post_thumbnail($post->ID, 'medium-large'); ?>
      <h1 class="h2">Contact</h1>
        <div class="contact-details">
          <?php
          $contact_info = get_field('contact_info', $post->ID);
          echo $contact_info['phone_number'] . '<br />';
          echo $contact_info['email'] . '<br />';
          echo $contact_info['address'] . '<br />';
          ?>
        </div>
      </div>
    </div>
    <div class="col-lg-6 bg-lighter ">
      <div class="form-container d-flex align-items-center justify-content-center">
        <?php echo do_shortcode('[contact-form-7 id="57" title="Contact Form"]'); ?>
      </div>
    </div>
  </div>
  <?php echo wp_footer(); ?>
