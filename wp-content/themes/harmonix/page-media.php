<?php
/* Template name: Media Page */

  include 'parts/includes/header.php';

  $args = [
    'post_type' => 'post',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC',
    'post_status' => 'publish',
  ];

  $news = new WP_Query($args);

?>


<div class="grid-page-container bg-light">
  <div class="container-fluid">
    <h1 class="col-sm-12 page-title">Media</h1>
     <div class="row no-gutters articles">
      <?php if($news->have_posts()): while($news->have_posts()): $news->the_post();
        get_template_part( 'parts/includes/media-loop' );
      endwhile; endif; ?>
  </div>
</div>

<?php include 'parts/includes/footer.php'; ?>
