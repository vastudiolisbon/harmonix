  <?php include "parts/includes/header.php"; ?>
    <section class="dark">
      <div class="container-fluid ">
        <div class="wrapper mh-100 text-center">
          <div class="text-hero ">
            <h1 class="headline">Page not found</h1>
          </div>
          <p>
            <a href="<?php echo home_url(); ?>">Back to homepage</a>
          </p>
        </div>
      </div>
    </section>
  <?php include "parts/includes/footer.php" ?>
