<?php

function load_theme_scripts()
{

	wp_deregister_script('jquery');
	wp_enqueue_script('main-scripts', get_template_directory_uri() . '/dist/js/bundle.js', '', '', true);
	wp_enqueue_style('style', get_stylesheet_uri());
	wp_enqueue_style('main-styles', get_template_directory_uri() . '/dist/css/main.min.css');
}
add_action('wp_enqueue_scripts', 'load_theme_scripts');



include 'includes/filters.php';
include 'includes/cpts.php';
include 'includes/remove.php';
include 'includes/acfs.php';
include 'includes/menus.php';
include 'includes/layout_columns.php';

add_theme_support('post-thumbnails');

// add_image_size('card-espetaculo', 496, 385, true);

if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' 	=> 'Social Media',
		'menu_title'	=> 'Social Media',
		'menu_slug' 	=> 'social-media-settings',
		'capability'	=> 'edit_posts',
		'icon_url' => 'dashicons-share',
		'position' => 30,
		'redirect'		=> false
	));
}
