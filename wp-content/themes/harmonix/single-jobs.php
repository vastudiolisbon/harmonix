<?php include 'parts/includes/header.php'; ?>
<div class="grid-page-container dark">
  <div class="container-fluid">
    <a class="arrow-left" href="<?php echo get_post_type_archive_link('jobs'); ?>">
      <img src="<?php echo get_template_directory_uri() . '/assets/img/arrow.svg'; ?>" alt="archive-link">
      View all Jobs
    </a>
    <div class="row">
      <div class="job-description">
        <div class="team-member-info">
          <h1><?php echo get_the_title($post->ID); ?></h1>
          <?php if(!empty(get_field('company', $post->ID) )){ ?>
            <h3><?php echo get_field('company', $post->ID); ?></h3>
          <?php } ?>
          <div class="section-info">
            <h3 class="h6">Job:</h3>
            <p><?php echo get_field('job', $post->ID); ?></p>
            <br>

            <?php if(! empty( get_field('years_experience', $post->ID) )){ ?>
              <p>Years of Experience:</p>
              <p><?php echo get_field('years_expericence', $post->ID); ?></p>
            <?php } ?>
          </div>

          <?php
            if(!empty( get_field('application_link', $post->ID) ) ):
              $job_link = get_field('application_link', $post->ID);
            elseif( !empty( get_field('application_email') ) ) :
              $job_link = 'mailto:'.get_field('application_email');
            else: 
              $job_link = 'mailto:cofield@harmonixfund.com';
            endif;
          ?>
          <a class="pill pill-light" href="<?php echo $job_link; ?>" target="_blank" rel="noopener">
            Apply
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include 'parts/includes/footer.php'; ?>
