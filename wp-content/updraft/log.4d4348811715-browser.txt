0000.040 (R) [notice] Looking for db archive: file name: backup_2024-02-29-1051_Harmonix_7895dc037284-db.gz
0000.041 (R) [notice] Archive is expected to be size: 2862.4 KB: OK
0000.060 (R) [notice] Will not delete any archives after unpacking them, because there was no cloud storage for this backup
0000.068 (R) [notice] Unpacking backup... (backup_2024-02-29-1051_Harmonix_7895dc037284-db.gz, 2.8 Mb)
0000.423 (R) [notice] Restoring the database (on a large site this can take a long time - if it times out (which can happen if your web hosting company has configured your hosting to limit resources) then you should use a different method, such as phpMyAdmin)...
0000.514 (R) [notice] Enabling Maintenance mode&#8230;
0000.571 (R) [notice] Backup of: https://harmonixfund.com
0000.588 (R) [notice] Content URL: https://harmonixfund.com/wp-content
0000.590 (R) [notice] Uploads URL: https://harmonixfund.com/wp-content/uploads
0000.592 (R) [notice] Old table prefix: hnx_
0000.595 (R) [notice] Old ABSPATH: /home/jop9g8gor5uk/public_html/harmonixfund.com/
0000.597 (R) [notice] UpdraftPlus plugin slug: updraftplus/updraftplus.php
0000.600 (R) [notice] Site information: multisite = 0
0000.602 (R) [notice] Site information: sql_mode = NO_ENGINE_SUBSTITUTION
0000.607 (R) [notice] New table prefix: s1_
0000.622 (R) [notice] Processing table (InnoDB):  hnx_options - will restore as: s1_options
0000.918 (R) [notice] Atomic restore: dropping original table (hnx_options)
0000.942 (R) [notice] Atomic restore: renaming new table (s1_options) to final table name (hnx_options)
0001.493 (R) [notice] Search and replacing table: hnx_options: rows: 500
0001.646 (R) [notice] Processing table (InnoDB):  hnx_users - will restore as: s1_users
0001.896 (R) [notice] Atomic restore: dropping original table (hnx_users)
0001.913 (R) [notice] Atomic restore: renaming new table (s1_users) to final table name (hnx_users)
0001.930 (R) [notice] Search and replacing table: hnx_users: rows: 3
0001.936 (R) [notice] Processing table (InnoDB):  hnx_usermeta - will restore as: s1_usermeta
0001.999 (R) [notice] Atomic restore: dropping original table (hnx_usermeta)
0002.015 (R) [notice] Atomic restore: renaming new table (s1_usermeta) to final table name (hnx_usermeta)
0002.036 (R) [notice] Search and replacing table: hnx_usermeta: rows: 168
0002.042 (R) [notice] Processing table (MyISAM):  hnx_actionscheduler_actions - will restore as: s1_actionscheduler_actions
0002.195 (R) [notice] Atomic restore: dropping original table (hnx_actionscheduler_actions)
0002.207 (R) [notice] Atomic restore: renaming new table (s1_actionscheduler_actions) to final table name (hnx_actionscheduler_actions)
0002.233 (R) [notice] Search and replacing table: hnx_actionscheduler_actions: rows: 90
0002.244 (R) [notice] Processing table (MyISAM):  hnx_actionscheduler_claims - will restore as: s1_actionscheduler_claims
0002.275 (R) [notice] Atomic restore: dropping original table (hnx_actionscheduler_claims)
0002.285 (R) [notice] Atomic restore: renaming new table (s1_actionscheduler_claims) to final table name (hnx_actionscheduler_claims)
0002.310 (R) [notice] Search and replacing table: hnx_actionscheduler_claims: rows: 0
0002.312 (R) [notice] Processing table (MyISAM):  hnx_actionscheduler_groups - will restore as: s1_actionscheduler_groups
0002.357 (R) [notice] Atomic restore: dropping original table (hnx_actionscheduler_groups)
0002.366 (R) [notice] Atomic restore: renaming new table (s1_actionscheduler_groups) to final table name (hnx_actionscheduler_groups)
0002.386 (R) [notice] Search and replacing table: hnx_actionscheduler_groups: rows: 4
0002.391 (R) [notice] Processing table (MyISAM):  hnx_actionscheduler_logs - will restore as: s1_actionscheduler_logs
0002.643 (R) [notice] Atomic restore: dropping original table (hnx_actionscheduler_logs)
0002.651 (R) [notice] Atomic restore: renaming new table (s1_actionscheduler_logs) to final table name (hnx_actionscheduler_logs)
0002.672 (R) [notice] Search and replacing table: hnx_actionscheduler_logs: rows: 265
0002.677 (R) [notice] Processing table (InnoDB):  hnx_commentmeta - will restore as: s1_commentmeta
0002.728 (R) [notice] Atomic restore: dropping original table (hnx_commentmeta)
0002.743 (R) [notice] Atomic restore: renaming new table (s1_commentmeta) to final table name (hnx_commentmeta)
0002.762 (R) [notice] Search and replacing table: hnx_commentmeta: rows: 0
0002.764 (R) [notice] Processing table (InnoDB):  hnx_comments - will restore as: s1_comments
0002.840 (R) [notice] Atomic restore: dropping original table (hnx_comments)
0002.860 (R) [notice] Atomic restore: renaming new table (s1_comments) to final table name (hnx_comments)
0002.882 (R) [notice] Search and replacing table: hnx_comments: rows: 0
0002.884 (R) [notice] Processing table (InnoDB):  hnx_links - will restore as: s1_links
0002.936 (R) [notice] Atomic restore: dropping original table (hnx_links)
0002.951 (R) [notice] Atomic restore: renaming new table (s1_links) to final table name (hnx_links)
0002.969 (R) [notice] Search and replacing table: hnx_links: rows: 0
0002.972 (R) [notice] Processing table (InnoDB):  hnx_postmeta - will restore as: s1_postmeta
0004.834 (R) [notice] Database queries processed: 50 in 4.40 seconds
0004.835 (R) [notice] An error (1) occurred: - Variable 'sql_mode' can't be set to the value of 'NULL' - the database query being run was: /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
0004.846 (R) [notice] Atomic restore: dropping original table (hnx_postmeta)
0004.861 (R) [notice] Atomic restore: renaming new table (s1_postmeta) to final table name (hnx_postmeta)
0004.892 (R) [notice] Search and replacing table: hnx_postmeta: rows: 1480
0008.818 (R) [notice] Processing table (InnoDB):  hnx_posts - will restore as: s1_posts
0009.917 (R) [notice] Atomic restore: dropping original table (hnx_posts)
0009.936 (R) [notice] Atomic restore: renaming new table (s1_posts) to final table name (hnx_posts)
0009.958 (R) [notice] Search and replacing table: hnx_posts: rows: 1554
0012.448 (R) [notice] Processing table (InnoDB):  hnx_term_relationships - will restore as: s1_term_relationships
0012.519 (R) [notice] Atomic restore: dropping original table (hnx_term_relationships)
0012.539 (R) [notice] Atomic restore: renaming new table (s1_term_relationships) to final table name (hnx_term_relationships)
0012.569 (R) [notice] Skipping this table: data in this table (hnx_term_relationships) should not be search/replaced
0012.571 (R) [notice] Processing table (InnoDB):  hnx_term_taxonomy - will restore as: s1_term_taxonomy
0012.650 (R) [notice] Atomic restore: dropping original table (hnx_term_taxonomy)
0012.694 (R) [notice] Atomic restore: renaming new table (s1_term_taxonomy) to final table name (hnx_term_taxonomy)
0012.716 (R) [notice] Search and replacing table: hnx_term_taxonomy: rows: 36
0012.720 (R) [notice] Processing table (InnoDB):  hnx_termmeta - will restore as: s1_termmeta
0012.777 (R) [notice] Atomic restore: dropping original table (hnx_termmeta)
0012.794 (R) [notice] Atomic restore: renaming new table (s1_termmeta) to final table name (hnx_termmeta)
0012.814 (R) [notice] Search and replacing table: hnx_termmeta: rows: 24
0012.816 (R) [notice] Processing table (InnoDB):  hnx_terms - will restore as: s1_terms
0012.871 (R) [notice] Atomic restore: dropping original table (hnx_terms)
0012.887 (R) [notice] Atomic restore: renaming new table (s1_terms) to final table name (hnx_terms)
0012.907 (R) [notice] Search and replacing table: hnx_terms: rows: 36
0012.910 (R) [notice] Processing table (InnoDB):  hnx_aioseo_cache - will restore as: s1_aioseo_cache
0012.996 (R) [notice] Atomic restore: dropping original table (hnx_aioseo_cache)
0012.998 (R) [notice] Atomic restore: renaming new table (s1_aioseo_cache) to final table name (hnx_aioseo_cache)
0013.023 (R) [notice] Search and replacing table: hnx_aioseo_cache: rows: 16
0013.028 (R) [notice] Processing table (InnoDB):  hnx_aioseo_notifications - will restore as: s1_aioseo_notifications
0013.097 (R) [notice] Atomic restore: dropping original table (hnx_aioseo_notifications)
0013.100 (R) [notice] Atomic restore: renaming new table (s1_aioseo_notifications) to final table name (hnx_aioseo_notifications)
0013.120 (R) [notice] Search and replacing table: hnx_aioseo_notifications: rows: 8
0013.123 (R) [notice] Processing table (InnoDB):  hnx_aioseo_posts - will restore as: s1_aioseo_posts
0013.215 (R) [notice] Atomic restore: dropping original table (hnx_aioseo_posts)
0013.217 (R) [notice] Atomic restore: renaming new table (s1_aioseo_posts) to final table name (hnx_aioseo_posts)
0013.252 (R) [notice] Search and replacing table: hnx_aioseo_posts: rows: 55
0013.483 (R) [notice] Processing table (InnoDB):  hnx_redirects - will restore as: s1_redirects
0013.528 (R) [notice] Atomic restore: dropping original table (hnx_redirects)
0013.531 (R) [notice] Atomic restore: renaming new table (s1_redirects) to final table name (hnx_redirects)
0013.559 (R) [notice] Search and replacing table: hnx_redirects: rows: 1
0013.563 (R) [notice] Processing table (InnoDB):  hnx_rio_process_queue - will restore as: s1_rio_process_queue
0013.671 (R) [notice] Atomic restore: dropping original table (hnx_rio_process_queue)
0013.693 (R) [notice] Atomic restore: renaming new table (s1_rio_process_queue) to final table name (hnx_rio_process_queue)
0013.712 (R) [notice] Search and replacing table: hnx_rio_process_queue: rows: 2
0013.715 (R) [notice] Processing table (MyISAM):  hnx_wfblockediplog - will restore as: s1_wfblockediplog
0013.842 (R) [notice] Atomic restore: dropping original table (hnx_wfblockediplog)
0013.853 (R) [notice] Atomic restore: renaming new table (s1_wfblockediplog) to final table name (hnx_wfblockediplog)
0013.870 (R) [notice] Skipping this table: data in this table (hnx_wfblockediplog) should not be search/replaced
0013.871 (R) [notice] Processing table (MyISAM):  hnx_wfblocks7 - will restore as: s1_wfblocks7
0013.930 (R) [notice] Atomic restore: dropping original table (hnx_wfblocks7)
0013.943 (R) [notice] Atomic restore: renaming new table (s1_wfblocks7) to final table name (hnx_wfblocks7)
0013.965 (R) [notice] Search and replacing table: hnx_wfblocks7: rows: 11
0013.970 (R) [notice] Processing table (MyISAM):  hnx_wfconfig - will restore as: s1_wfconfig
0014.312 (R) [notice] Database queries processed: 100 in 13.88 seconds
0014.322 (R) [notice] Atomic restore: dropping original table (hnx_wfconfig)
0014.332 (R) [notice] Atomic restore: renaming new table (s1_wfconfig) to final table name (hnx_wfconfig)
0014.352 (R) [notice] Search and replacing table: hnx_wfconfig: rows: 282
0014.404 (R) [notice] Processing table (MyISAM):  hnx_wfcrawlers - will restore as: s1_wfcrawlers
0014.462 (R) [notice] Atomic restore: dropping original table (hnx_wfcrawlers)
0014.471 (R) [notice] Atomic restore: renaming new table (s1_wfcrawlers) to final table name (hnx_wfcrawlers)
0014.493 (R) [notice] Search and replacing table: hnx_wfcrawlers: rows: 22
0014.497 (R) [notice] Processing table (MyISAM):  hnx_wffilechanges - will restore as: s1_wffilechanges
0014.535 (R) [notice] Atomic restore: dropping original table (hnx_wffilechanges)
0014.544 (R) [notice] Atomic restore: renaming new table (s1_wffilechanges) to final table name (hnx_wffilechanges)
0014.578 (R) [notice] Search and replacing table: hnx_wffilechanges: rows: 0
0014.581 (R) [notice] Processing table (MyISAM):  hnx_wffilemods - will restore as: s1_wffilemods
0026.116 (R) [notice] An error (2) occurred: - Variable 'sql_mode' can't be set to the value of 'NULL' - the database query being run was: /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
0026.126 (R) [notice] Atomic restore: dropping original table (hnx_wffilemods)
0026.137 (R) [notice] Atomic restore: renaming new table (s1_wffilemods) to final table name (hnx_wffilemods)
0026.159 (R) [notice] Skipping this table: data in this table (hnx_wffilemods) should not be search/replaced
0026.160 (R) [notice] Processing table (MyISAM):  hnx_wfhits - will restore as: s1_wfhits
0027.274 (R) [notice] Atomic restore: dropping original table (hnx_wfhits)
0027.284 (R) [notice] Atomic restore: renaming new table (s1_wfhits) to final table name (hnx_wfhits)
0027.300 (R) [notice] Skipping this table: data in this table (hnx_wfhits) should not be search/replaced
0027.302 (R) [notice] Processing table (MyISAM):  hnx_wfhoover - will restore as: s1_wfhoover
0027.339 (R) [notice] Atomic restore: dropping original table (hnx_wfhoover)
0027.349 (R) [notice] Atomic restore: renaming new table (s1_wfhoover) to final table name (hnx_wfhoover)
0027.374 (R) [notice] Search and replacing table: hnx_wfhoover: rows: 0
0027.377 (R) [notice] Processing table (MyISAM):  hnx_wfissues - will restore as: s1_wfissues
0027.444 (R) [notice] Atomic restore: dropping original table (hnx_wfissues)
0027.453 (R) [notice] Database queries processed: 150 in 27.02 seconds
0027.454 (R) [notice] Atomic restore: renaming new table (s1_wfissues) to final table name (hnx_wfissues)
0027.475 (R) [notice] Search and replacing table: hnx_wfissues: rows: 8
0027.518 (R) [notice] Processing table (MyISAM):  hnx_wfknownfilelist - will restore as: s1_wfknownfilelist
0037.727 (R) [notice] An error (3) occurred: - Variable 'sql_mode' can't be set to the value of 'NULL' - the database query being run was: /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
0037.739 (R) [notice] Atomic restore: dropping original table (hnx_wfknownfilelist)
0037.752 (R) [notice] Atomic restore: renaming new table (s1_wfknownfilelist) to final table name (hnx_wfknownfilelist)
0037.779 (R) [notice] Search and replacing table: hnx_wfknownfilelist: rows: 13232
0064.969 (R) [notice] Searching and replacing reached row: 5000
0092.983 (R) [notice] Searching and replacing reached row: 10000
0111.548 (R) [notice] Processing table (MyISAM):  hnx_wflivetraffichuman - will restore as: s1_wflivetraffichuman
0111.597 (R) [notice] Atomic restore: dropping original table (hnx_wflivetraffichuman)
0111.609 (R) [notice] Atomic restore: renaming new table (s1_wflivetraffichuman) to final table name (hnx_wflivetraffichuman)
0111.639 (R) [notice] Search and replacing table: hnx_wflivetraffichuman: rows: 0
0111.641 (R) [notice] Processing table (MyISAM):  hnx_wflocs - will restore as: s1_wflocs
0111.677 (R) [notice] Atomic restore: dropping original table (hnx_wflocs)
0111.686 (R) [notice] Atomic restore: renaming new table (s1_wflocs) to final table name (hnx_wflocs)
0111.709 (R) [notice] Search and replacing table: hnx_wflocs: rows: 0
0111.712 (R) [notice] Processing table (MyISAM):  hnx_wflogins - will restore as: s1_wflogins
0111.875 (R) [notice] Atomic restore: dropping original table (hnx_wflogins)
0111.891 (R) [notice] Atomic restore: renaming new table (s1_wflogins) to final table name (hnx_wflogins)
0111.912 (R) [notice] Search and replacing table: hnx_wflogins: rows: 140
0111.917 (R) [notice] Processing table (InnoDB):  hnx_wfls_2fa_secrets - will restore as: s1_wfls_2fa_secrets
0111.963 (R) [notice] Atomic restore: dropping original table (hnx_wfls_2fa_secrets)
0111.980 (R) [notice] Atomic restore: renaming new table (s1_wfls_2fa_secrets) to final table name (hnx_wfls_2fa_secrets)
0111.997 (R) [notice] Search and replacing table: hnx_wfls_2fa_secrets: rows: 0
0112.000 (R) [notice] Processing table (MEMORY):  hnx_wfls_role_counts - will restore as: s1_wfls_role_counts
0112.017 (R) [notice] Atomic restore: dropping original table (hnx_wfls_role_counts)
0112.023 (R) [notice] Atomic restore: renaming new table (s1_wfls_role_counts) to final table name (hnx_wfls_role_counts)
0112.031 (R) [notice] Search and replacing table: hnx_wfls_role_counts: rows: 0
0112.034 (R) [notice] Processing table (InnoDB):  hnx_wfls_settings - will restore as: s1_wfls_settings
0112.078 (R) [notice] Atomic restore: dropping original table (hnx_wfls_settings)
0112.108 (R) [notice] Atomic restore: renaming new table (s1_wfls_settings) to final table name (hnx_wfls_settings)
0112.124 (R) [notice] Search and replacing table: hnx_wfls_settings: rows: 30
0112.127 (R) [notice] Processing table (MyISAM):  hnx_wfnotifications - will restore as: s1_wfnotifications
0112.219 (R) [notice] Atomic restore: dropping original table (hnx_wfnotifications)
0112.229 (R) [notice] Atomic restore: renaming new table (s1_wfnotifications) to final table name (hnx_wfnotifications)
0112.254 (R) [notice] Search and replacing table: hnx_wfnotifications: rows: 70
0112.549 (R) [notice] Processing table (MyISAM):  hnx_wfpendingissues - will restore as: s1_wfpendingissues
0112.608 (R) [notice] Atomic restore: dropping original table (hnx_wfpendingissues)
0112.616 (R) [notice] Atomic restore: renaming new table (s1_wfpendingissues) to final table name (hnx_wfpendingissues)
0112.640 (R) [notice] Search and replacing table: hnx_wfpendingissues: rows: 0
0112.642 (R) [notice] Processing table (MyISAM):  hnx_wfreversecache - will restore as: s1_wfreversecache
0112.679 (R) [notice] Atomic restore: dropping original table (hnx_wfreversecache)
0112.688 (R) [notice] Atomic restore: renaming new table (s1_wfreversecache) to final table name (hnx_wfreversecache)
0112.709 (R) [notice] Search and replacing table: hnx_wfreversecache: rows: 1
0112.715 (R) [notice] Processing table (InnoDB):  hnx_wfsecurityevents - will restore as: s1_wfsecurityevents
0112.755 (R) [notice] Atomic restore: dropping original table (hnx_wfsecurityevents)
0112.772 (R) [notice] Atomic restore: renaming new table (s1_wfsecurityevents) to final table name (hnx_wfsecurityevents)
0112.793 (R) [notice] Search and replacing table: hnx_wfsecurityevents: rows: 0
0112.795 (R) [notice] Processing table (MyISAM):  hnx_wfsnipcache - will restore as: s1_wfsnipcache
0112.831 (R) [notice] Atomic restore: dropping original table (hnx_wfsnipcache)
0112.841 (R) [notice] Atomic restore: renaming new table (s1_wfsnipcache) to final table name (hnx_wfsnipcache)
0112.866 (R) [notice] Search and replacing table: hnx_wfsnipcache: rows: 0
0112.868 (R) [notice] Processing table (MyISAM):  hnx_wfstatus - will restore as: s1_wfstatus
0113.680 (R) [notice] Atomic restore: dropping original table (hnx_wfstatus)
0113.691 (R) [notice] Atomic restore: renaming new table (s1_wfstatus) to final table name (hnx_wfstatus)
0113.712 (R) [notice] Search and replacing table: hnx_wfstatus: rows: 933
0113.721 (R) [notice] Processing table (MyISAM):  hnx_wftrafficrates - will restore as: s1_wftrafficrates
0113.755 (R) [notice] Atomic restore: dropping original table (hnx_wftrafficrates)
0113.764 (R) [notice] Atomic restore: renaming new table (s1_wftrafficrates) to final table name (hnx_wftrafficrates)
0113.788 (R) [notice] Search and replacing table: hnx_wftrafficrates: rows: 0
0113.790 (R) [notice] Processing table (InnoDB):  hnx_wfwaffailures - will restore as: s1_wfwaffailures
0113.831 (R) [notice] Atomic restore: dropping original table (hnx_wfwaffailures)
0113.847 (R) [notice] Atomic restore: renaming new table (s1_wfwaffailures) to final table name (hnx_wfwaffailures)
0113.867 (R) [notice] Search and replacing table: hnx_wfwaffailures: rows: 0
0113.870 (R) [notice] Processing table (InnoDB):  hnx_wpforms_payment_meta - will restore as: s1_wpforms_payment_meta
0113.928 (R) [notice] Atomic restore: dropping original table (hnx_wpforms_payment_meta)
0113.930 (R) [notice] Database queries processed: 200 in 113.50 seconds
0113.932 (R) [notice] Atomic restore: renaming new table (s1_wpforms_payment_meta) to final table name (hnx_wpforms_payment_meta)
0113.949 (R) [notice] Search and replacing table: hnx_wpforms_payment_meta: rows: 0
0113.951 (R) [notice] Processing table (InnoDB):  hnx_wpforms_payments - will restore as: s1_wpforms_payments
0114.049 (R) [notice] Atomic restore: dropping original table (hnx_wpforms_payments)
0114.052 (R) [notice] Atomic restore: renaming new table (s1_wpforms_payments) to final table name (hnx_wpforms_payments)
0114.074 (R) [notice] Search and replacing table: hnx_wpforms_payments: rows: 0
0114.077 (R) [notice] Processing table (InnoDB):  hnx_wpforms_tasks_meta - will restore as: s1_wpforms_tasks_meta
0114.117 (R) [notice] Atomic restore: dropping original table (hnx_wpforms_tasks_meta)
0114.120 (R) [notice] Atomic restore: renaming new table (s1_wpforms_tasks_meta) to final table name (hnx_wpforms_tasks_meta)
0114.142 (R) [notice] Search and replacing table: hnx_wpforms_tasks_meta: rows: 4
0114.145 (R) [notice] Processing table (InnoDB):  hnx_wpmailsmtp_debug_events - will restore as: s1_wpmailsmtp_debug_events
0114.185 (R) [notice] Atomic restore: dropping original table (hnx_wpmailsmtp_debug_events)
0114.199 (R) [notice] Atomic restore: renaming new table (s1_wpmailsmtp_debug_events) to final table name (hnx_wpmailsmtp_debug_events)
0114.216 (R) [notice] Search and replacing table: hnx_wpmailsmtp_debug_events: rows: 0
0114.219 (R) [notice] Processing table (MyISAM):  hnx_wpmailsmtp_tasks_meta - will restore as: s1_wpmailsmtp_tasks_meta
0114.259 (R) [notice] Atomic restore: dropping original table (hnx_wpmailsmtp_tasks_meta)
0114.272 (R) [notice] Atomic restore: renaming new table (s1_wpmailsmtp_tasks_meta) to final table name (hnx_wpmailsmtp_tasks_meta)
0114.294 (R) [notice] Search and replacing table: hnx_wpmailsmtp_tasks_meta: rows: 1
0114.299 (R) [notice] Processing table (InnoDB):  hnx_yoast_indexable - will restore as: s1_yoast_indexable
0114.403 (R) [notice] Atomic restore: dropping original table (hnx_yoast_indexable)
0114.422 (R) [notice] Atomic restore: renaming new table (s1_yoast_indexable) to final table name (hnx_yoast_indexable)
0114.448 (R) [notice] Search and replacing table: hnx_yoast_indexable: rows: 112
0114.826 (R) [notice] Processing table (InnoDB):  hnx_yoast_indexable_hierarchy - will restore as: s1_yoast_indexable_hierarchy
0114.894 (R) [notice] Atomic restore: dropping original table (hnx_yoast_indexable_hierarchy)
0114.909 (R) [notice] Atomic restore: renaming new table (s1_yoast_indexable_hierarchy) to final table name (hnx_yoast_indexable_hierarchy)
0114.929 (R) [notice] Search and replacing table: hnx_yoast_indexable_hierarchy: rows: 108
0114.932 (R) [notice] Processing table (InnoDB):  hnx_yoast_migrations - will restore as: s1_yoast_migrations
0114.985 (R) [notice] Atomic restore: dropping original table (hnx_yoast_migrations)
0115.000 (R) [notice] Atomic restore: renaming new table (s1_yoast_migrations) to final table name (hnx_yoast_migrations)
0115.021 (R) [notice] Search and replacing table: hnx_yoast_migrations: rows: 24
0115.024 (R) [notice] Processing table (InnoDB):  hnx_yoast_primary_term - will restore as: s1_yoast_primary_term
0115.079 (R) [notice] Atomic restore: dropping original table (hnx_yoast_primary_term)
0115.095 (R) [notice] Atomic restore: renaming new table (s1_yoast_primary_term) to final table name (hnx_yoast_primary_term)
0115.117 (R) [notice] Search and replacing table: hnx_yoast_primary_term: rows: 13
0115.120 (R) [notice] Processing table (InnoDB):  hnx_yoast_seo_links - will restore as: s1_yoast_seo_links
0115.175 (R) [notice] Disabling Maintenance mode&#8230;
0115.178 (R) [notice] Atomic restore: dropping original table (hnx_yoast_seo_links)
0115.192 (R) [notice] Atomic restore: renaming new table (s1_yoast_seo_links) to final table name (hnx_yoast_seo_links)
0115.210 (R) [notice] Search and replacing table: hnx_yoast_seo_links: rows: 59
0115.284 (R) [notice] Finished: lines processed: 227 in 114.85 seconds
0115.289 (R) [notice] Cleaning up rubbish...
0115.294 (R) [notice] Database search and replace: replace https://harmonixfund.com in backup dump with http://localhost:11600
0115.295 (R) [notice] Database search and replace: replace http://www.harmonixfund.com in backup dump with http://localhost:11600
0115.297 (R) [notice] Database search and replace: replace http://harmonixfund.com in backup dump with http://localhost:11600
0115.298 (R) [notice] Database search and replace: replace https://www.harmonixfund.com in backup dump with http://localhost:11600
0115.299 (R) [notice] Database search and replace: replace /home/jop9g8gor5uk/public_html/harmonixfund.com in backup dump with /var/www/html
0115.302 (R) [notice] Search and replacing table: hnx_actionscheduler_actions: already done
0115.303 (R) [notice] Search and replacing table: hnx_actionscheduler_claims: already done
0115.304 (R) [notice] Search and replacing table: hnx_actionscheduler_groups: already done
0115.306 (R) [notice] Search and replacing table: hnx_actionscheduler_logs: already done
0115.309 (R) [notice] Search and replacing table: hnx_aioseo_cache: already done
0115.311 (R) [notice] Search and replacing table: hnx_aioseo_notifications: already done
0115.312 (R) [notice] Search and replacing table: hnx_aioseo_posts: already done
0115.314 (R) [notice] Search and replacing table: hnx_commentmeta: already done
0115.315 (R) [notice] Search and replacing table: hnx_comments: already done
0115.317 (R) [notice] Search and replacing table: hnx_links: already done
0115.318 (R) [notice] Search and replacing table: hnx_options: already done
0115.319 (R) [notice] Search and replacing table: hnx_postmeta: already done
0115.321 (R) [notice] Search and replacing table: hnx_posts: already done
0115.322 (R) [notice] Search and replacing table: hnx_redirects: already done
0115.325 (R) [notice] Search and replacing table: hnx_rio_process_queue: already done
0115.326 (R) [notice] Search and replacing table: hnx_term_relationships: already done
0115.327 (R) [notice] Search and replacing table: hnx_term_taxonomy: already done
0115.329 (R) [notice] Search and replacing table: hnx_termmeta: already done
0115.330 (R) [notice] Search and replacing table: hnx_terms: already done
0115.331 (R) [notice] Search and replacing table: hnx_usermeta: already done
0115.333 (R) [notice] Search and replacing table: hnx_users: already done
0115.336 (R) [notice] Search and replacing table: hnx_wfblockediplog: already done
0115.337 (R) [notice] Search and replacing table: hnx_wfblocks7: already done
0115.339 (R) [notice] Search and replacing table: hnx_wfconfig: already done
0115.340 (R) [notice] Search and replacing table: hnx_wfcrawlers: already done
0115.342 (R) [notice] Search and replacing table: hnx_wffilechanges: already done
0115.343 (R) [notice] Search and replacing table: hnx_wffilemods: already done
0115.345 (R) [notice] Search and replacing table: hnx_wfhits: already done
0115.347 (R) [notice] Search and replacing table: hnx_wfhoover: already done
0115.348 (R) [notice] Search and replacing table: hnx_wfissues: already done
0115.349 (R) [notice] Search and replacing table: hnx_wfknownfilelist: already done
0115.351 (R) [notice] Search and replacing table: hnx_wflivetraffichuman: already done
0115.352 (R) [notice] Search and replacing table: hnx_wflocs: already done
0115.353 (R) [notice] Search and replacing table: hnx_wflogins: already done
0115.356 (R) [notice] Search and replacing table: hnx_wfls_2fa_secrets: already done
0115.357 (R) [notice] Search and replacing table: hnx_wfls_role_counts: already done
0115.358 (R) [notice] Search and replacing table: hnx_wfls_settings: already done
0115.359 (R) [notice] Search and replacing table: hnx_wfnotifications: already done
0115.362 (R) [notice] Search and replacing table: hnx_wfpendingissues: already done
0115.363 (R) [notice] Search and replacing table: hnx_wfreversecache: already done
0115.365 (R) [notice] Search and replacing table: hnx_wfsecurityevents: already done
0115.367 (R) [notice] Search and replacing table: hnx_wfsnipcache: already done
0115.368 (R) [notice] Search and replacing table: hnx_wfstatus: already done
0115.369 (R) [notice] Search and replacing table: hnx_wftrafficrates: already done
0115.370 (R) [notice] Search and replacing table: hnx_wfwaffailures: already done
0115.372 (R) [notice] Search and replacing table: hnx_wpforms_payment_meta: already done
0115.373 (R) [notice] Search and replacing table: hnx_wpforms_payments: already done
0115.377 (R) [notice] Search and replacing table: hnx_wpforms_tasks_meta: already done
0115.378 (R) [notice] Search and replacing table: hnx_wpmailsmtp_debug_events: already done
0115.380 (R) [notice] Search and replacing table: hnx_wpmailsmtp_tasks_meta: already done
0115.381 (R) [notice] Search and replacing table: hnx_yoast_indexable: already done
0115.382 (R) [notice] Search and replacing table: hnx_yoast_indexable_hierarchy: already done
0115.384 (R) [notice] Search and replacing table: hnx_yoast_migrations: already done
0115.385 (R) [notice] Search and replacing table: hnx_yoast_primary_term: already done
0115.386 (R) [notice] Search and replacing table: hnx_yoast_seo_links: already done
0115.389 (R) [notice] Tables examined: 51
0115.390 (R) [notice] Rows examined: 19292
0115.391 (R) [notice] Changes made: 15243
0115.391 (R) [notice] SQL update commands run: 15206
0115.392 (R) [notice] Errors: 0
0115.393 (R) [notice] Time taken (seconds): 81.726
0115.458 (R) [notice] Plugin path eps-301-redirects/eps-301-redirects.php not found: de-activating.
0115.459 (R) [notice] Plugin path google-analytics-for-wordpress/googleanalytics.php not found: de-activating.
0115.460 (R) [notice] Plugin path google-site-kit/google-site-kit.php not found: de-activating.
0115.462 (R) [notice] Plugin path optinmonster/optin-monster-wp-api.php not found: de-activating.
0115.464 (R) [notice] Plugin path wp-mail-smtp/wp_mail_smtp.php not found: de-activating.
0115.465 (R) [notice] Plugin path wpforms-lite/wpforms.php not found: de-activating.
0115.815 (R) [notice] Restore successful!
