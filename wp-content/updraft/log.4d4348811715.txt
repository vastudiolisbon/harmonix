0000.012 () Opened log file at time: Thu, 29 Feb 2024 11:05:53 +0000 on http://localhost:11600
0000.014 () UpdraftPlus WordPress backup plugin (https://updraftplus.com): 1.24.1 WP: 6.4.3 PHP: 7.4.33 (fpm-fcgi, Linux be5fd7f15908 5.15.49-linuxkit #1 SMP PREEMPT Tue Sep 13 07:51:32 UTC 2022 aarch64) MySQL: 8.0.32 (max packet size=67108864) WPLANG: en_US Server: nginx/1.25.1 safe_mode: 0 max_execution_time: 900 memory_limit: 256M (used: 11.3M | 4M) multisite: N openssl: OpenSSL 1.1.1n  15 Mar 2022 mcrypt: N LANG:  ZipArchive::addFile: Y
0000.016 () Free space on disk containing Updraft's temporary directory: 8450.2 MB
0000.060 () Restore setup, now closing connection and starting restore over AJAX.
0000.010 () Opened log file at time: Thu, 29 Feb 2024 11:05:53 +0000 on http://localhost:11600
0000.012 () UpdraftPlus WordPress backup plugin (https://updraftplus.com): 1.24.1 WP: 6.4.3 PHP: 7.4.33 (fpm-fcgi, Linux be5fd7f15908 5.15.49-linuxkit #1 SMP PREEMPT Tue Sep 13 07:51:32 UTC 2022 aarch64) MySQL: 8.0.32 (max packet size=67108864) WPLANG: en_US Server: nginx/1.25.1 safe_mode: 0 max_execution_time: 900 memory_limit: 256M (used: 9.4M | 2M) multisite: N openssl: OpenSSL 1.1.1n  15 Mar 2022 mcrypt: N LANG:  ZipArchive::addFile: Y
0000.014 () Free space on disk containing Updraft's temporary directory: 8450.2 MB
0000.015 () Ensuring WP_Filesystem is setup for a restore
0000.025 () WP_Filesystem is setup and ready for a restore
0000.034 () Restore job started. Entities to restore: db. Restore options: {"updraft_restorer_replacesiteurl":"1","include_unspecified_tables":false,"tables_to_restore":["hnx_options","hnx_users","hnx_usermeta","hnx_actionscheduler_actions","hnx_actionscheduler_claims","hnx_actionscheduler_groups","hnx_actionscheduler_logs","hnx_commentmeta","hnx_comments","hnx_links","hnx_postmeta","hnx_posts","hnx_term_relationships","hnx_term_taxonomy","hnx_termmeta","hnx_terms","hnx_aioseo_cache","hnx_aioseo_notifications","hnx_aioseo_posts","hnx_redirects","hnx_rio_process_queue","hnx_wfblockediplog","hnx_wfblocks7","hnx_wfconfig","hnx_wfcrawlers","hnx_wffilechanges","hnx_wffilemods","hnx_wfhits","hnx_wfhoover","hnx_wfissues","hnx_wfknownfilelist","hnx_wflivetraffichuman","hnx_wflocs","hnx_wflogins","hnx_wfls_2fa_secrets","hnx_wfls_role_counts","hnx_wfls_settings","hnx_wfnotifications","hnx_wfpendingissues","hnx_wfreversecache","hnx_wfsecurityevents","hnx_wfsnipcache","hnx_wfstatus","hnx_wftrafficrates","hnx_wfwaffailures","hnx_wpforms_payment_meta","hnx_wpforms_payments","hnx_wpforms_tasks_meta","hnx_wpmailsmtp_debug_events","hnx_wpmailsmtp_tasks_meta","hnx_yoast_indexable","hnx_yoast_indexable_hierarchy","hnx_yoast_migrations","hnx_yoast_primary_term","hnx_yoast_seo_links"],"tables_to_skip":[],"updraft_encryptionphrase":"","updraft_restorer_wpcore_includewpconfig":false,"updraft_incremental_restore_point":-1}
0000.059 () Will not delete any archives after unpacking them, because there was no cloud storage for this backup
0000.066 () Entity: db
0000.067 () restore_backup(backup_file=backup_2024-02-29-1051_Harmonix_7895dc037284-db.gz, type=db, info=a:0:{}, last_one=1)
0000.068 () Unpacking backup... (backup_2024-02-29-1051_Harmonix_7895dc037284-db.gz, 2.8 Mb)
0000.420 () Database successfully unpacked
0000.421 () Restoring the database (on a large site this can take a long time - if it times out (which can happen if your web hosting company has configured your hosting to limit resources) then you should use a different method, such as phpMyAdmin)...
0000.425 () Using direct MySQL access; value of use_mysqli is: 1
0000.426 () SQL compatibility mode is: NO_AUTO_VALUE_ON_ZERO,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
0000.512 () Max packet size: 64 MB
0000.512 () Entering maintenance mode
0000.513 () Enabling Maintenance mode&#8230;
0000.570 () Backup of: https://harmonixfund.com
0000.589 () Content URL: https://harmonixfund.com/wp-content
0000.591 () Uploads URL: https://harmonixfund.com/wp-content/uploads
0000.593 () Old table prefix: hnx_
0000.596 () Old ABSPATH: /home/jop9g8gor5uk/public_html/harmonixfund.com/
0000.598 () UpdraftPlus plugin slug: updraftplus/updraftplus.php
0000.601 () Site information: multisite=0
0000.603 () Site information: sql_mode=NO_ENGINE_SUBSTITUTION
0000.606 () New table prefix: s1_
0000.621 () Processing table (InnoDB): hnx_options - will restore as: s1_options
0000.917 () Atomic restore: dropping original table (hnx_options)
0000.941 () Atomic restore: renaming new table (s1_options) to final table name (hnx_options)
0000.963 () Restoring prior UD configuration (table: hnx_options; keys: 102)
0001.494 () Search and replacing table: hnx_options: rows: 500
0001.644 () Processing table (InnoDB): hnx_users - will restore as: s1_users
0001.895 () Atomic restore: dropping original table (hnx_users)
0001.912 () Atomic restore: renaming new table (s1_users) to final table name (hnx_users)
0001.931 () Search and replacing table: hnx_users: rows: 3
0001.935 () Processing table (InnoDB): hnx_usermeta - will restore as: s1_usermeta
0001.999 () Atomic restore: dropping original table (hnx_usermeta)
0002.015 () Atomic restore: renaming new table (s1_usermeta) to final table name (hnx_usermeta)
0002.036 () Search and replacing table: hnx_usermeta: rows: 168
0002.042 () Processing table (MyISAM): hnx_actionscheduler_actions - will restore as: s1_actionscheduler_actions
0002.194 () Atomic restore: dropping original table (hnx_actionscheduler_actions)
0002.206 () Atomic restore: renaming new table (s1_actionscheduler_actions) to final table name (hnx_actionscheduler_actions)
0002.234 () Search and replacing table: hnx_actionscheduler_actions: rows: 90
0002.241 () Incomplete object detected in database: ActionScheduler_IntervalSchedule; Search and replace will be skipped for these entries
0002.241 () Incomplete object detected in database: ActionScheduler_SimpleSchedule; Search and replace will be skipped for these entries
0002.242 () Incomplete object detected in database: ActionScheduler_NullSchedule; Search and replace will be skipped for these entries
0002.244 () Processing table (MyISAM): hnx_actionscheduler_claims - will restore as: s1_actionscheduler_claims
0002.275 () Atomic restore: dropping original table (hnx_actionscheduler_claims)
0002.284 () Atomic restore: renaming new table (s1_actionscheduler_claims) to final table name (hnx_actionscheduler_claims)
0002.310 () Search and replacing table: hnx_actionscheduler_claims: rows: 0
0002.311 () Processing table (MyISAM): hnx_actionscheduler_groups - will restore as: s1_actionscheduler_groups
0002.357 () Atomic restore: dropping original table (hnx_actionscheduler_groups)
0002.365 () Atomic restore: renaming new table (s1_actionscheduler_groups) to final table name (hnx_actionscheduler_groups)
0002.386 () Search and replacing table: hnx_actionscheduler_groups: rows: 4
0002.391 () Processing table (MyISAM): hnx_actionscheduler_logs - will restore as: s1_actionscheduler_logs
0002.642 () Atomic restore: dropping original table (hnx_actionscheduler_logs)
0002.651 () Atomic restore: renaming new table (s1_actionscheduler_logs) to final table name (hnx_actionscheduler_logs)
0002.672 () Search and replacing table: hnx_actionscheduler_logs: rows: 265
0002.677 () Processing table (InnoDB): hnx_commentmeta - will restore as: s1_commentmeta
0002.728 () Atomic restore: dropping original table (hnx_commentmeta)
0002.742 () Atomic restore: renaming new table (s1_commentmeta) to final table name (hnx_commentmeta)
0002.762 () Search and replacing table: hnx_commentmeta: rows: 0
0002.763 () Processing table (InnoDB): hnx_comments - will restore as: s1_comments
0002.840 () Atomic restore: dropping original table (hnx_comments)
0002.860 () Atomic restore: renaming new table (s1_comments) to final table name (hnx_comments)
0002.883 () Search and replacing table: hnx_comments: rows: 0
0002.883 () Processing table (InnoDB): hnx_links - will restore as: s1_links
0002.927 () Skipped execution of SQL statement (unwanted or internally handled type=18): /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
0002.935 () Atomic restore: dropping original table (hnx_links)
0002.950 () Atomic restore: renaming new table (s1_links) to final table name (hnx_links)
0002.970 () Search and replacing table: hnx_links: rows: 0
0002.971 () Processing table (InnoDB): hnx_postmeta - will restore as: s1_postmeta
0004.833 () Database queries processed: 50 in 4.40 seconds
0004.835 () An error (1) occurred: Variable 'sql_mode' can't be set to the value of 'NULL' - SQL query was (type=0): /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
0004.845 () Atomic restore: dropping original table (hnx_postmeta)
0004.860 () Atomic restore: renaming new table (s1_postmeta) to final table name (hnx_postmeta)
0004.893 () Search and replacing table: hnx_postmeta: rows: 1480
0008.817 () Processing table (InnoDB): hnx_posts - will restore as: s1_posts
0009.917 () Atomic restore: dropping original table (hnx_posts)
0009.935 () Atomic restore: renaming new table (s1_posts) to final table name (hnx_posts)
0009.956 () Skipping search/replace on GUID column in posts table
0009.959 () Search and replacing table: hnx_posts: rows: 1554
0012.448 () Processing table (InnoDB): hnx_term_relationships - will restore as: s1_term_relationships
0012.518 () Atomic restore: dropping original table (hnx_term_relationships)
0012.538 () Atomic restore: renaming new table (s1_term_relationships) to final table name (hnx_term_relationships)
0012.568 () Skipping this table: data in this table (hnx_term_relationships) should not be search/replaced
0012.571 () Processing table (InnoDB): hnx_term_taxonomy - will restore as: s1_term_taxonomy
0012.649 () Atomic restore: dropping original table (hnx_term_taxonomy)
0012.693 () Atomic restore: renaming new table (s1_term_taxonomy) to final table name (hnx_term_taxonomy)
0012.717 () Search and replacing table: hnx_term_taxonomy: rows: 36
0012.719 () Processing table (InnoDB): hnx_termmeta - will restore as: s1_termmeta
0012.776 () Atomic restore: dropping original table (hnx_termmeta)
0012.793 () Atomic restore: renaming new table (s1_termmeta) to final table name (hnx_termmeta)
0012.814 () Search and replacing table: hnx_termmeta: rows: 24
0012.816 () Processing table (InnoDB): hnx_terms - will restore as: s1_terms
0012.870 () Atomic restore: dropping original table (hnx_terms)
0012.887 () Atomic restore: renaming new table (s1_terms) to final table name (hnx_terms)
0012.908 () Search and replacing table: hnx_terms: rows: 36
0012.910 () Processing table (InnoDB): hnx_aioseo_cache - will restore as: s1_aioseo_cache
0012.995 () Atomic restore: dropping original table (hnx_aioseo_cache)
0012.998 () Atomic restore: renaming new table (s1_aioseo_cache) to final table name (hnx_aioseo_cache)
0013.023 () Search and replacing table: hnx_aioseo_cache: rows: 16
0013.027 () Processing table (InnoDB): hnx_aioseo_notifications - will restore as: s1_aioseo_notifications
0013.096 () Atomic restore: dropping original table (hnx_aioseo_notifications)
0013.098 () Atomic restore: renaming new table (s1_aioseo_notifications) to final table name (hnx_aioseo_notifications)
0013.121 () Search and replacing table: hnx_aioseo_notifications: rows: 8
0013.122 () Processing table (InnoDB): hnx_aioseo_posts - will restore as: s1_aioseo_posts
0013.215 () Atomic restore: dropping original table (hnx_aioseo_posts)
0013.217 () Atomic restore: renaming new table (s1_aioseo_posts) to final table name (hnx_aioseo_posts)
0013.253 () Search and replacing table: hnx_aioseo_posts: rows: 55
0013.483 () Processing table (InnoDB): hnx_redirects - will restore as: s1_redirects
0013.528 () Atomic restore: dropping original table (hnx_redirects)
0013.530 () Atomic restore: renaming new table (s1_redirects) to final table name (hnx_redirects)
0013.560 () Search and replacing table: hnx_redirects: rows: 1
0013.562 () Processing table (InnoDB): hnx_rio_process_queue - will restore as: s1_rio_process_queue
0013.670 () Atomic restore: dropping original table (hnx_rio_process_queue)
0013.692 () Atomic restore: renaming new table (s1_rio_process_queue) to final table name (hnx_rio_process_queue)
0013.713 () Search and replacing table: hnx_rio_process_queue: rows: 2
0013.715 () Processing table (MyISAM): hnx_wfblockediplog - will restore as: s1_wfblockediplog
0013.842 () Atomic restore: dropping original table (hnx_wfblockediplog)
0013.852 () Atomic restore: renaming new table (s1_wfblockediplog) to final table name (hnx_wfblockediplog)
0013.869 () Skipping this table: data in this table (hnx_wfblockediplog) should not be search/replaced
0013.871 () Processing table (MyISAM): hnx_wfblocks7 - will restore as: s1_wfblocks7
0013.929 () Atomic restore: dropping original table (hnx_wfblocks7)
0013.942 () Atomic restore: renaming new table (s1_wfblocks7) to final table name (hnx_wfblocks7)
0013.966 () Search and replacing table: hnx_wfblocks7: rows: 11
0013.969 () Processing table (MyISAM): hnx_wfconfig - will restore as: s1_wfconfig
0014.311 () Database queries processed: 100 in 13.88 seconds
0014.321 () Atomic restore: dropping original table (hnx_wfconfig)
0014.330 () Atomic restore: renaming new table (s1_wfconfig) to final table name (hnx_wfconfig)
0014.353 () Search and replacing table: hnx_wfconfig: rows: 282
0014.403 () Processing table (MyISAM): hnx_wfcrawlers - will restore as: s1_wfcrawlers
0014.461 () Atomic restore: dropping original table (hnx_wfcrawlers)
0014.471 () Atomic restore: renaming new table (s1_wfcrawlers) to final table name (hnx_wfcrawlers)
0014.494 () Search and replacing table: hnx_wfcrawlers: rows: 22
0014.497 () Processing table (MyISAM): hnx_wffilechanges - will restore as: s1_wffilechanges
0014.525 () Skipped execution of SQL statement (unwanted or internally handled type=18): /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
0014.533 () Atomic restore: dropping original table (hnx_wffilechanges)
0014.543 () Atomic restore: renaming new table (s1_wffilechanges) to final table name (hnx_wffilechanges)
0014.578 () Search and replacing table: hnx_wffilechanges: rows: 0
0014.580 () Processing table (MyISAM): hnx_wffilemods - will restore as: s1_wffilemods
0026.117 () An error (2) occurred: Variable 'sql_mode' can't be set to the value of 'NULL' - SQL query was (type=0): /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
0026.126 () Atomic restore: dropping original table (hnx_wffilemods)
0026.137 () Atomic restore: renaming new table (s1_wffilemods) to final table name (hnx_wffilemods)
0026.158 () Skipping this table: data in this table (hnx_wffilemods) should not be search/replaced
0026.160 () Processing table (MyISAM): hnx_wfhits - will restore as: s1_wfhits
0027.273 () Atomic restore: dropping original table (hnx_wfhits)
0027.283 () Atomic restore: renaming new table (s1_wfhits) to final table name (hnx_wfhits)
0027.299 () Skipping this table: data in this table (hnx_wfhits) should not be search/replaced
0027.301 () Processing table (MyISAM): hnx_wfhoover - will restore as: s1_wfhoover
0027.339 () Atomic restore: dropping original table (hnx_wfhoover)
0027.348 () Atomic restore: renaming new table (s1_wfhoover) to final table name (hnx_wfhoover)
0027.375 () Search and replacing table: hnx_wfhoover: rows: 0
0027.376 () Processing table (MyISAM): hnx_wfissues - will restore as: s1_wfissues
0027.432 () Skipped execution of SQL statement (unwanted or internally handled type=18): /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
0027.443 () Atomic restore: dropping original table (hnx_wfissues)
0027.452 () Database queries processed: 150 in 27.02 seconds
0027.454 () Atomic restore: renaming new table (s1_wfissues) to final table name (hnx_wfissues)
0027.475 () Search and replacing table: hnx_wfissues: rows: 8
0027.517 () Processing table (MyISAM): hnx_wfknownfilelist - will restore as: s1_wfknownfilelist
0037.728 () An error (3) occurred: Variable 'sql_mode' can't be set to the value of 'NULL' - SQL query was (type=0): /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
0037.738 () Atomic restore: dropping original table (hnx_wfknownfilelist)
0037.751 () Atomic restore: renaming new table (s1_wfknownfilelist) to final table name (hnx_wfknownfilelist)
0037.779 () Search and replacing table: hnx_wfknownfilelist: rows: 13232
0064.968 () Searching and replacing reached row: 5000
0092.982 () Searching and replacing reached row: 10000
0111.546 () Processing table (MyISAM): hnx_wflivetraffichuman - will restore as: s1_wflivetraffichuman
0111.596 () Atomic restore: dropping original table (hnx_wflivetraffichuman)
0111.608 () Atomic restore: renaming new table (s1_wflivetraffichuman) to final table name (hnx_wflivetraffichuman)
0111.639 () Search and replacing table: hnx_wflivetraffichuman: rows: 0
0111.640 () Processing table (MyISAM): hnx_wflocs - will restore as: s1_wflocs
0111.676 () Atomic restore: dropping original table (hnx_wflocs)
0111.684 () Atomic restore: renaming new table (s1_wflocs) to final table name (hnx_wflocs)
0111.711 () Search and replacing table: hnx_wflocs: rows: 0
0111.711 () Processing table (MyISAM): hnx_wflogins - will restore as: s1_wflogins
0111.874 () Atomic restore: dropping original table (hnx_wflogins)
0111.890 () Atomic restore: renaming new table (s1_wflogins) to final table name (hnx_wflogins)
0111.912 () Search and replacing table: hnx_wflogins: rows: 140
0111.916 () Processing table (InnoDB): hnx_wfls_2fa_secrets - will restore as: s1_wfls_2fa_secrets
0111.963 () Atomic restore: dropping original table (hnx_wfls_2fa_secrets)
0111.979 () Atomic restore: renaming new table (s1_wfls_2fa_secrets) to final table name (hnx_wfls_2fa_secrets)
0111.998 () Search and replacing table: hnx_wfls_2fa_secrets: rows: 0
0112.000 () Processing table (MEMORY): hnx_wfls_role_counts - will restore as: s1_wfls_role_counts
0112.016 () Atomic restore: dropping original table (hnx_wfls_role_counts)
0112.022 () Atomic restore: renaming new table (s1_wfls_role_counts) to final table name (hnx_wfls_role_counts)
0112.033 () Search and replacing table: hnx_wfls_role_counts: rows: 0
0112.034 () Processing table (InnoDB): hnx_wfls_settings - will restore as: s1_wfls_settings
0112.077 () Atomic restore: dropping original table (hnx_wfls_settings)
0112.106 () Atomic restore: renaming new table (s1_wfls_settings) to final table name (hnx_wfls_settings)
0112.125 () Search and replacing table: hnx_wfls_settings: rows: 30
0112.126 () Processing table (MyISAM): hnx_wfnotifications - will restore as: s1_wfnotifications
0112.217 () Atomic restore: dropping original table (hnx_wfnotifications)
0112.228 () Atomic restore: renaming new table (s1_wfnotifications) to final table name (hnx_wfnotifications)
0112.256 () Search and replacing table: hnx_wfnotifications: rows: 70
0112.549 () Processing table (MyISAM): hnx_wfpendingissues - will restore as: s1_wfpendingissues
0112.607 () Atomic restore: dropping original table (hnx_wfpendingissues)
0112.616 () Atomic restore: renaming new table (s1_wfpendingissues) to final table name (hnx_wfpendingissues)
0112.641 () Search and replacing table: hnx_wfpendingissues: rows: 0
0112.641 () Processing table (MyISAM): hnx_wfreversecache - will restore as: s1_wfreversecache
0112.679 () Atomic restore: dropping original table (hnx_wfreversecache)
0112.687 () Atomic restore: renaming new table (s1_wfreversecache) to final table name (hnx_wfreversecache)
0112.710 () Search and replacing table: hnx_wfreversecache: rows: 1
0112.714 () Processing table (InnoDB): hnx_wfsecurityevents - will restore as: s1_wfsecurityevents
0112.754 () Atomic restore: dropping original table (hnx_wfsecurityevents)
0112.771 () Atomic restore: renaming new table (s1_wfsecurityevents) to final table name (hnx_wfsecurityevents)
0112.793 () Search and replacing table: hnx_wfsecurityevents: rows: 0
0112.794 () Processing table (MyISAM): hnx_wfsnipcache - will restore as: s1_wfsnipcache
0112.830 () Atomic restore: dropping original table (hnx_wfsnipcache)
0112.841 () Atomic restore: renaming new table (s1_wfsnipcache) to final table name (hnx_wfsnipcache)
0112.867 () Search and replacing table: hnx_wfsnipcache: rows: 0
0112.867 () Processing table (MyISAM): hnx_wfstatus - will restore as: s1_wfstatus
0113.679 () Atomic restore: dropping original table (hnx_wfstatus)
0113.689 () Atomic restore: renaming new table (s1_wfstatus) to final table name (hnx_wfstatus)
0113.713 () Search and replacing table: hnx_wfstatus: rows: 933
0113.721 () Processing table (MyISAM): hnx_wftrafficrates - will restore as: s1_wftrafficrates
0113.753 () Atomic restore: dropping original table (hnx_wftrafficrates)
0113.763 () Atomic restore: renaming new table (s1_wftrafficrates) to final table name (hnx_wftrafficrates)
0113.788 () Search and replacing table: hnx_wftrafficrates: rows: 0
0113.789 () Processing table (InnoDB): hnx_wfwaffailures - will restore as: s1_wfwaffailures
0113.830 () Atomic restore: dropping original table (hnx_wfwaffailures)
0113.847 () Atomic restore: renaming new table (s1_wfwaffailures) to final table name (hnx_wfwaffailures)
0113.869 () Search and replacing table: hnx_wfwaffailures: rows: 0
0113.869 () Processing table (InnoDB): hnx_wpforms_payment_meta - will restore as: s1_wpforms_payment_meta
0113.927 () Atomic restore: dropping original table (hnx_wpforms_payment_meta)
0113.930 () Database queries processed: 200 in 113.50 seconds
0113.931 () Atomic restore: renaming new table (s1_wpforms_payment_meta) to final table name (hnx_wpforms_payment_meta)
0113.950 () Search and replacing table: hnx_wpforms_payment_meta: rows: 0
0113.951 () Processing table (InnoDB): hnx_wpforms_payments - will restore as: s1_wpforms_payments
0114.049 () Atomic restore: dropping original table (hnx_wpforms_payments)
0114.051 () Atomic restore: renaming new table (s1_wpforms_payments) to final table name (hnx_wpforms_payments)
0114.075 () Search and replacing table: hnx_wpforms_payments: rows: 0
0114.076 () Processing table (InnoDB): hnx_wpforms_tasks_meta - will restore as: s1_wpforms_tasks_meta
0114.117 () Atomic restore: dropping original table (hnx_wpforms_tasks_meta)
0114.119 () Atomic restore: renaming new table (s1_wpforms_tasks_meta) to final table name (hnx_wpforms_tasks_meta)
0114.143 () Search and replacing table: hnx_wpforms_tasks_meta: rows: 4
0114.145 () Processing table (InnoDB): hnx_wpmailsmtp_debug_events - will restore as: s1_wpmailsmtp_debug_events
0114.184 () Atomic restore: dropping original table (hnx_wpmailsmtp_debug_events)
0114.198 () Atomic restore: renaming new table (s1_wpmailsmtp_debug_events) to final table name (hnx_wpmailsmtp_debug_events)
0114.217 () Search and replacing table: hnx_wpmailsmtp_debug_events: rows: 0
0114.218 () Processing table (MyISAM): hnx_wpmailsmtp_tasks_meta - will restore as: s1_wpmailsmtp_tasks_meta
0114.258 () Atomic restore: dropping original table (hnx_wpmailsmtp_tasks_meta)
0114.271 () Atomic restore: renaming new table (s1_wpmailsmtp_tasks_meta) to final table name (hnx_wpmailsmtp_tasks_meta)
0114.295 () Search and replacing table: hnx_wpmailsmtp_tasks_meta: rows: 1
0114.298 () Processing table (InnoDB): hnx_yoast_indexable - will restore as: s1_yoast_indexable
0114.402 () Atomic restore: dropping original table (hnx_yoast_indexable)
0114.422 () Atomic restore: renaming new table (s1_yoast_indexable) to final table name (hnx_yoast_indexable)
0114.449 () Search and replacing table: hnx_yoast_indexable: rows: 112
0114.825 () Processing table (InnoDB): hnx_yoast_indexable_hierarchy - will restore as: s1_yoast_indexable_hierarchy
0114.893 () Atomic restore: dropping original table (hnx_yoast_indexable_hierarchy)
0114.908 () Atomic restore: renaming new table (s1_yoast_indexable_hierarchy) to final table name (hnx_yoast_indexable_hierarchy)
0114.930 () Search and replacing table: hnx_yoast_indexable_hierarchy: rows: 108
0114.931 () Processing table (InnoDB): hnx_yoast_migrations - will restore as: s1_yoast_migrations
0114.985 () Atomic restore: dropping original table (hnx_yoast_migrations)
0115.000 () Atomic restore: renaming new table (s1_yoast_migrations) to final table name (hnx_yoast_migrations)
0115.022 () Search and replacing table: hnx_yoast_migrations: rows: 24
0115.023 () Processing table (InnoDB): hnx_yoast_primary_term - will restore as: s1_yoast_primary_term
0115.078 () Atomic restore: dropping original table (hnx_yoast_primary_term)
0115.093 () Atomic restore: renaming new table (s1_yoast_primary_term) to final table name (hnx_yoast_primary_term)
0115.118 () Search and replacing table: hnx_yoast_primary_term: rows: 13
0115.119 () Processing table (InnoDB): hnx_yoast_seo_links - will restore as: s1_yoast_seo_links
0115.174 () Unlocking database and leaving maintenance mode
0115.175 () Disabling Maintenance mode&#8230;
0115.178 () Atomic restore: dropping original table (hnx_yoast_seo_links)
0115.192 () Atomic restore: renaming new table (s1_yoast_seo_links) to final table name (hnx_yoast_seo_links)
0115.210 () Search and replacing table: hnx_yoast_seo_links: rows: 59
0115.283 () Finished: lines processed: 227 in 114.85 seconds
0115.288 () Cleaning up rubbish...
0115.292 () Begin search and replace (updraftplus_restored_db)
0115.292 () [Database-replace-site-url] Database: search and replace site URL
0115.293 () Database search and replace: replace https://harmonixfund.com in backup dump with http://localhost:11600
0115.295 () Database search and replace: replace http://www.harmonixfund.com in backup dump with http://localhost:11600
0115.296 () Database search and replace: replace http://harmonixfund.com in backup dump with http://localhost:11600
0115.297 () Database search and replace: replace https://www.harmonixfund.com in backup dump with http://localhost:11600
0115.298 () Database search and replace: replace /home/jop9g8gor5uk/public_html/harmonixfund.com in backup dump with /var/www/html
0115.302 () Search and replacing table: hnx_actionscheduler_actions: already done
0115.304 () Search and replacing table: hnx_actionscheduler_claims: already done
0115.305 () Search and replacing table: hnx_actionscheduler_groups: already done
0115.306 () Search and replacing table: hnx_actionscheduler_logs: already done
0115.310 () Search and replacing table: hnx_aioseo_cache: already done
0115.311 () Search and replacing table: hnx_aioseo_notifications: already done
0115.312 () Search and replacing table: hnx_aioseo_posts: already done
0115.315 () Search and replacing table: hnx_commentmeta: already done
0115.316 () Search and replacing table: hnx_comments: already done
0115.317 () Search and replacing table: hnx_links: already done
0115.319 () Search and replacing table: hnx_options: already done
0115.320 () Search and replacing table: hnx_postmeta: already done
0115.322 () Search and replacing table: hnx_posts: already done
0115.324 () Search and replacing table: hnx_redirects: already done
0115.325 () Search and replacing table: hnx_rio_process_queue: already done
0115.327 () Search and replacing table: hnx_term_relationships: already done
0115.328 () Search and replacing table: hnx_term_taxonomy: already done
0115.330 () Search and replacing table: hnx_termmeta: already done
0115.331 () Search and replacing table: hnx_terms: already done
0115.332 () Search and replacing table: hnx_usermeta: already done
0115.334 () Search and replacing table: hnx_users: already done
0115.337 () Search and replacing table: hnx_wfblockediplog: already done
0115.338 () Search and replacing table: hnx_wfblocks7: already done
0115.340 () Search and replacing table: hnx_wfconfig: already done
0115.341 () Search and replacing table: hnx_wfcrawlers: already done
0115.342 () Search and replacing table: hnx_wffilechanges: already done
0115.344 () Search and replacing table: hnx_wffilemods: already done
0115.346 () Search and replacing table: hnx_wfhits: already done
0115.347 () Search and replacing table: hnx_wfhoover: already done
0115.349 () Search and replacing table: hnx_wfissues: already done
0115.350 () Search and replacing table: hnx_wfknownfilelist: already done
0115.352 () Search and replacing table: hnx_wflivetraffichuman: already done
0115.353 () Search and replacing table: hnx_wflocs: already done
0115.355 () Search and replacing table: hnx_wflogins: already done
0115.356 () Search and replacing table: hnx_wfls_2fa_secrets: already done
0115.357 () Search and replacing table: hnx_wfls_role_counts: already done
0115.359 () Search and replacing table: hnx_wfls_settings: already done
0115.360 () Search and replacing table: hnx_wfnotifications: already done
0115.362 () Search and replacing table: hnx_wfpendingissues: already done
0115.365 () Search and replacing table: hnx_wfreversecache: already done
0115.366 () Search and replacing table: hnx_wfsecurityevents: already done
0115.367 () Search and replacing table: hnx_wfsnipcache: already done
0115.368 () Search and replacing table: hnx_wfstatus: already done
0115.370 () Search and replacing table: hnx_wftrafficrates: already done
0115.371 () Search and replacing table: hnx_wfwaffailures: already done
0115.373 () Search and replacing table: hnx_wpforms_payment_meta: already done
0115.375 () Search and replacing table: hnx_wpforms_payments: already done
0115.377 () Search and replacing table: hnx_wpforms_tasks_meta: already done
0115.379 () Search and replacing table: hnx_wpmailsmtp_debug_events: already done
0115.380 () Search and replacing table: hnx_wpmailsmtp_tasks_meta: already done
0115.382 () Search and replacing table: hnx_yoast_indexable: already done
0115.383 () Search and replacing table: hnx_yoast_indexable_hierarchy: already done
0115.385 () Search and replacing table: hnx_yoast_migrations: already done
0115.386 () Search and replacing table: hnx_yoast_primary_term: already done
0115.388 () Search and replacing table: hnx_yoast_seo_links: already done
0115.457 () Plugin path eps-301-redirects/eps-301-redirects.php not found: de-activating.
0115.458 () Plugin path google-analytics-for-wordpress/googleanalytics.php not found: de-activating.
0115.460 () Plugin path google-site-kit/google-site-kit.php not found: de-activating.
0115.461 () Plugin path optinmonster/optin-monster-wp-api.php not found: de-activating.
0115.463 () Plugin path wp-mail-smtp/wp_mail_smtp.php not found: de-activating.
0115.464 () Plugin path wpforms-lite/wpforms.php not found: de-activating.
0115.477 () Purging cache directory: /var/www/html/wp-content/cache
0115.814 () Restore successful!
0115.815 () Restore successful
